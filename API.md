# API Endpoints

## Users

---
> GET: /users

* Requires a user session.
* Retrieves metadata about all users on the platform.

### Response Body:

```typescript
{
    users: [
        {
            fullName: "Madeup Personname",
            email: "email@domain.com"
        },
        {
            ...
        }
    ]
}
```

---
> POST: /users

* Registers a user.

### Request Body:

```typescript
{
    email: "user@email.com";
    password: "password";
    fullName: "Personwit Madeupnem";
}
```

## Sessions

---
> GET: /sessions

* Requires a user session.
* Returns metadata about the current user session.

### Response Body:

```typescript
{
    fullName: "Person Madeupson";
    email: "user@email.com";
}
```

---
> POST: /sessions

* Authenticates a user and creates a session (i.e. log in).

### Request Body:

```typescript
{
    email: "user@email.com";
    password: "password";
}
```

---
> DELETE: /sessions

* Requires a user session.
* Deletes the current user session (i.e. log out).

## Tasks

---
> GET: /tasks

* Requires a user session.
* Retrieves a list of existing tasks, which can optionally be filtered.

### Query Parameters:

```typescript
{
    assignees: ["User 1", "User 2", "..."];      // ?assignees=Assignee 1&assignees=...  | Filter tasks by assignee
    author: "Author";                            // ?author=Author                       | Filter tasks by author
    status: "NOT_DONE" | "IN_PROGRESS" | "DONE"; // ?status=NOT_DONE                     | Filter tasks by status
    overdue: "NOT_OVERDUE" | "OVERDUE";          // ?overdue=OVERDUE                     | Filter (near) overdue tasks
    timeLeft: "P1DT1H30M";                       // ?time-left=P1DT                      | Max. time left till overdue
}
```

#### NOTE:

`timeLeft` specifies the maximum duration that can be left until a tasks due date for it to be considered near overdue.
Example: when `timeLeft` is specified as `P1DT12H` (Time Range of 1 Day and 12 Hours), then all tasks that will be
overdue in the next 36 Hours will be considered **near overdue** and match the filter. `timeLeft` must be specified
according to **ISO 8601 duration format**.

### Response Body:

```typescript
{
    tasks: [
        {
            id: "task-id",
            title: "title",
            due: "1970-01-01T00:00:00Z",
            status: "NOT_DONE" | "IN_PROGRESS" | "DONE"
        },
        {
            ...
        }
    ]
}
```

#### NOTE:

The `due` date is in **ISO 8601 time format**.

---
> GET: /tasks/{taskId}

* Requires a user session.
* Retrieves all details about a specific task by its task ID.

### Response Body:

```typescript
{
    task: {
        id: "task-id";
        title: "title";
        description: "description";
        due: "1970-01-01T00:00:00Z";
        assignees: ["Assignee 1", "Assignee 2", "..."];
        author: "Author";
        status: "NOT_DONE" | "IN_PROGRESS" | "DONE";
    }
}
```

---
> POST: /tasks

* Requires a user session.
* Creates a new task.
* Returns the ID the newly created task.

### Request Body:

```typescript
{
    task: {
        title: "title";
        description: "description";
        due: "1970-01-01T00:00:00Z";                    // (Optional)
        assignees: ["Assignee 1", "Assignee 2", "..."]; // (Optional)
        status: "NOT_DONE" | "IN_PROGRESS" | "DONE";    // (Optional)
    }
}
```

### Response Body:

```typescript
{
    taskId: "task-id";
}
```

---
> PUT: /tasks/{taskId}

* Requires a user session. The user must either be the author of the task or an assignee.
* Updates the specified task.

### Request Body:

```typescript
{
    assignees: ["Assignee 1", "Assignee 2", "..."]; // (Optional)
    status: "NOT_DONE" | "IN_PROGRESS" | "DONE";    // (Optional)
}
```

---
> DELETE: /tasks/{taskId}

* Requires a user session. The user must be the author of the task.
* Deletes the task with the specified ID.
