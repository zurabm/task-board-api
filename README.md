# Task Board

## Description

This is the REST API for Task Board, a small utility application for managing tasks.

## User Stories

> As a **user** I want ability to register in the system by entering full name, e-mail
> and password (no e-mail confirmations are required).

> As a **user** I want to log in to the system with my e-mail and password.

> As a **user** I want to see list of tasks created by me or assigned to me.

> As a **user** I want to be able to create new task by entering task title,
> description and due date

> As a **user** I want to be able to assign task to one or more other users.

> As a **user** I want to be able to change task status assigned to me (NOT DONE, IN
> PROGRESS, DONE).

> As a **user** I want to be able to see near due or overdue tasks highlighted in my task list.

> As a **user** I want to be able to filter task list by assignees, statuses or overdue.

> As a **user** I want to be able to delete tasks from list.

## Technical Details

### Backend

* Rest API
* Java / Spring Boot
* Any database
* Domain Driven Design
* Layered architecture
* Dependency injection
* Basic unit testing

### Frontend

* React
* OAuth/JWT or other authentication
* Convenient UX

## Configuration

Example configuration that uses an in-memory H2 database.

```properties
spring.datasource.url=jdbc:h2:mem:test
spring.profiles.active=persistence,security
spring.jpa.hibernate.ddl-auto=update
```
