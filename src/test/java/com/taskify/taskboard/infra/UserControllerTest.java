package com.taskify.taskboard.infra;

import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.clients.UserClient;
import com.taskify.taskboard.infra.configurations.InMemoryConfiguration;
import com.taskify.taskboard.infra.configurations.NoSecurityConfiguration;
import com.taskify.taskboard.infra.configurations.TaskBoardConfiguration;
import com.taskify.taskboard.infra.controllers.UserController;
import com.taskify.taskboard.infra.controllers.models.UserMetadataModel;
import com.taskify.taskboard.infra.controllers.responses.GetUsersResponse;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.taskify.taskboard.ResponseParser.parseResponse;
import static com.taskify.taskboard.TestAssertions.expectStatus;
import static com.taskify.taskboard.infra.configurations.Profiles.IN_MEMORY_PROFILE;
import static com.taskify.taskboard.infra.configurations.Profiles.NO_SECURITY_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest
@ContextConfiguration(classes = {
        UserController.class,
        TaskBoardConfiguration.class,
        NoSecurityConfiguration.class,
        InMemoryConfiguration.class,
})
@ActiveProfiles({IN_MEMORY_PROFILE, NO_SECURITY_PROFILE})
class UserControllerTest {
    @Autowired
    private MockMvc mvc;
    private UserClient userClient;

    @Autowired
    private InMemoryUserRepository userRepository;

    @BeforeEach
    void setUp() {
        userClient = UserClient.from(mvc);
        userRepository.clear();
    }

    @Test
    void testShouldRegisterUser() throws Exception {
        User user = User.builder().email("and@email.com").password("with pass").fullName("Some user").build();

        MvcResult result = userClient.registerUser(user.email(), user.password(), user.fullName());

        assertThat(userRepository.getUserByEmail(user.email())).isEqualTo(user);
        expectStatus(result, HttpStatus.CREATED);
    }

    @Test
    void testShouldGetUsers() throws Exception {
        User user1 = User.builder().email("user1@email.com").password("with pass").fullName("Some user 1").build();
        User user2 = User.builder().email("user2@email.com").password("with pass").fullName("Some user 2").build();
        User user3 = User.builder().email("user3@email.com").password("with pass").fullName("Some user 3").build();

        userRepository.addUser(user1);
        userRepository.addUser(user2);
        userRepository.addUser(user3);

        MvcResult result = userClient.getUsers();

        assertThat(parseResponse(result, GetUsersResponse.class).getUsers()).containsExactlyInAnyOrder(
                UserMetadataModel.from(user1),
                UserMetadataModel.from(user2),
                UserMetadataModel.from(user3)
        );
        expectStatus(result, HttpStatus.OK);
    }
}
