package com.taskify.taskboard.infra.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.infra.controllers.models.TaskModel;
import com.taskify.taskboard.infra.controllers.requests.CreateTaskRequest;
import com.taskify.taskboard.infra.controllers.requests.UpdateTaskRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;

public record TaskClient(MockMvc mvc) {
    /**
     * Submits a GET request to retrieve all tasks.
     */
    public MvcResult getTasks() throws Exception {
        return getTasks(new LinkedMultiValueMap<>());
    }

    /**
     * Submits a GET request to retrieve all tasks matching the specified filter.
     */
    public MvcResult getTasks(MultiValueMap<String, String> queryParams) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/tasks").queryParams(queryParams)).andReturn();
    }

    /**
     * Submits a GET request to retrieve the task with the specified ID.
     */
    public MvcResult getTask(String taskId) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/tasks/{taskId}", taskId)).andReturn();
    }

    /**
     * Submits a POST request to create a new task.
     */
    public MvcResult createTask(TaskModel taskModel) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post("/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new CreateTaskRequest(taskModel)))
        ).andReturn();
    }

    /**
     * Submits a PUT request to update an existing task. Only assignees are updated.
     */
    public MvcResult updateTask(String taskId, List<String> assignees) throws Exception {
        return updateTask(taskId, assignees, null);
    }

    /**
     * Submits a PUT request to update an existing task. Only the status is updated.
     */
    public MvcResult updateTask(String taskId, Task.Status status) throws Exception {
        return updateTask(taskId, null, status);
    }

    /**
     * Submits a PUT request to update an existing task.
     */
    public MvcResult updateTask(String taskId, List<String> assignees, Task.Status status) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.put("/tasks/{taskId}", taskId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new UpdateTaskRequest(assignees, status)))
        ).andReturn();
    }

    /**
     * Submits a DELETE request to delete the specified task.
     */
    public MvcResult deleteTask(String taskId) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.delete("/tasks/{taskId}", taskId)).andReturn();
    }

    public static TaskClient from(MockMvc mvc) {
        return new TaskClient(mvc);
    }
}
