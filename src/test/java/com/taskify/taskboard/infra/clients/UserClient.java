package com.taskify.taskboard.infra.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskify.taskboard.infra.controllers.requests.RegisterUserRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public record UserClient(MockMvc mvc) {
    /**
     * Submits a POST request to register a user with the specified details.
     */
    public MvcResult registerUser(String email, String password, String fullName) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(RegisterUserRequest.builder()
                        .email(email)
                        .password(password)
                        .fullName(fullName)
                        .build()
                ))
        ).andReturn();
    }

    /**
     * Submits a GET request to retrieve public information on all users on the platform.
     */
    public MvcResult getUsers() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/users")).andReturn();
    }

    public static UserClient from(MockMvc mvc) {
        return new UserClient(mvc);
    }
}
