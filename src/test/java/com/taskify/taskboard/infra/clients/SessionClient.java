package com.taskify.taskboard.infra.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.taskify.taskboard.infra.controllers.requests.CreateSessionRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public record SessionClient(MockMvc mvc) {
    /**
     * Submits a POST request to create a user session.
     */
    public MvcResult createSession(String email, String password) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post("/sessions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new CreateSessionRequest(email, password)))
        ).andReturn();
    }

    /**
     * Submits a GET request to retrieve information on the current session.
     */
    public MvcResult getSession() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get("/sessions")).andReturn();
    }

    /**
     * Submits a DELETE request to remove the current user session.
     */
    public MvcResult deleteSession() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.delete("/sessions")).andReturn();
    }

    public static SessionClient from(MockMvc mvc) {
        return new SessionClient(mvc);
    }
}
