package com.taskify.taskboard.infra.clients.rest;

import com.taskify.taskboard.infra.cookies.Cookies;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class RestClient {
    public static final String COOKIE_HEADER = "Cookie";
    public static final String SET_COOKIE_HEADER = "set-cookie";

    /**
     * Submits a POST request to the specified URL.
     */
    public <ResponseT, RequestT> ResponseEntity<ResponseT> post(
            String url, RequestT requestBody, Class<ResponseT> responseClass
    ) {
        return exchange(HttpMethod.POST, url, requestBody, responseClass);
    }

    /**
     * Submits a GET request to the specified URL.
     */
    public <ResponseT> ResponseEntity<ResponseT> get(String url, Class<ResponseT> responseClass) {
        return exchange(HttpMethod.GET, url, null, responseClass);
    }

    /**
     * Submits a DELETE request to the specified URL.
     */
    public <ResponseT> ResponseEntity<ResponseT> delete(String url, Class<ResponseT> responseClass) {
        return exchange(HttpMethod.DELETE, url, null, responseClass);
    }

    /**
     * Creates a generic REST client that will submit requests to the specified port of localhost.
     */
    public static RestClient forPort(int port) {
        return new RestClient(port);
    }

    private RestClient(int port) {
        this.port = port;
    }

    /**
     * Submits a request with the given method to the specified URL.
     */
    private <ResponseT, RequestT> ResponseEntity<ResponseT> exchange(
            HttpMethod method, String url, RequestT requestBody, Class<ResponseT> responseClass
    ) {
        ResponseEntity<ResponseT> response = new RestTemplate().exchange(
                "http://localhost:{port}" + url,
                method,
                new HttpEntity<>(requestBody, withCookieHeader(new HttpHeaders())),
                responseClass,
                port
        );

        storeCookies(response);

        return response;
    }

    /**
     * Stores cookies from the specified {@link ResponseEntity}.
     */
    private void storeCookies(ResponseEntity<?> response) {

        List<String> cookieStrings = response.getHeaders().get(SET_COOKIE_HEADER);

        if (cookieStrings != null) {
            cookieHeader = Cookies.toCookieHeader(cookieStrings);
        }
    }

    /**
     * Adds the most recently stored cookies (if any) to the specified {@link HttpHeaders}.
     */
    private HttpHeaders withCookieHeader(HttpHeaders headers) {
        if (cookieHeader != null) {
            headers.add(COOKIE_HEADER, cookieHeader);
        }
        return headers;
    }

    private String cookieHeader;
    private final int port;
}
