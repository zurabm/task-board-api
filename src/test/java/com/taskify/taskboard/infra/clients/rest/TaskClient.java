package com.taskify.taskboard.infra.clients.rest;

import com.taskify.taskboard.infra.controllers.responses.GetTasksResponse;
import org.springframework.http.ResponseEntity;

public record TaskClient(RestClient client) {
    /**
     * Submits a POST request to authenticate the user and create a session,
     */
    public ResponseEntity<GetTasksResponse> getTasks() {
        return client.get("/tasks", GetTasksResponse.class);
    }
}
