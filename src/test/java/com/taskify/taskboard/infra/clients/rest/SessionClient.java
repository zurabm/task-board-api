package com.taskify.taskboard.infra.clients.rest;

import com.taskify.taskboard.infra.controllers.requests.CreateSessionRequest;
import com.taskify.taskboard.infra.controllers.responses.GetSessionResponse;
import org.springframework.http.ResponseEntity;

public record SessionClient(RestClient client) {
    /**
     * Submits a GET request to retrieve information on the current session.
     */
    public ResponseEntity<GetSessionResponse> getSession() {
        return client.get("/sessions", GetSessionResponse.class);
    }

    /**
     * Submits a POST request to authenticate the user and create a session,
     */
    public ResponseEntity<Void> createSession(String email, String password) {
        return client.post("/sessions", new CreateSessionRequest(email, password), Void.class);
    }

    /**
     * Submits a DELETE request to remove the current user session.
     */
    public ResponseEntity<Void> deleteSession() {
        return client.delete("/sessions", Void.class);
    }
}
