package com.taskify.taskboard.infra;

import com.taskify.taskboard.TaskBoardApplication;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.clients.rest.RestClient;
import com.taskify.taskboard.infra.clients.rest.SessionClient;
import com.taskify.taskboard.infra.clients.rest.TaskClient;
import com.taskify.taskboard.infra.controllers.responses.GetSessionResponse;
import com.taskify.taskboard.infra.controllers.responses.GetTasksResponse;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.PostConstruct;

import static com.taskify.taskboard.TestAssertions.expectStatus;
import static com.taskify.taskboard.infra.configurations.Profiles.IN_MEMORY_PROFILE;
import static com.taskify.taskboard.infra.configurations.Profiles.SECURITY_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {
        TaskBoardApplication.class,
        SessionControllerTest.Configuration.class
})
@ActiveProfiles({IN_MEMORY_PROFILE, SECURITY_PROFILE})
class SessionControllerTest {
    @Value("${local.server.port}")
    private int port;

    private SessionClient sessionClient;
    private TaskClient taskClient;

    @BeforeEach
    void setUp() {
        RestClient restClient = RestClient.forPort(port);
        sessionClient = new SessionClient(restClient);
        taskClient = new TaskClient(restClient);
    }

    @Test
    void testShouldAuthenticateUser() {
        ResponseEntity<Void> response = sessionClient.createSession("user@email.com", "User password");

        expectStatus(response, HttpStatus.CREATED);
    }

    @Test
    void testShouldNotAuthenticateNonExistentUser() {
        try {
            sessionClient.createSession("not.user@email.com", "User password");
            fail("An exception should have been thrown.");
        } catch (HttpClientErrorException exception) {
            expectStatus(exception, HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void testShouldCreateSessionAfterAuthenticatingUser() {
        sessionClient.createSession("user@email.com", "User password");
        ResponseEntity<GetTasksResponse> response = taskClient.getTasks(); // Example request.

        expectStatus(response, HttpStatus.OK);
    }

    @Test
    void testShouldReturnSessionInformation() {
        sessionClient.createSession("user@email.com", "User password");
        ResponseEntity<GetSessionResponse> response = sessionClient.getSession();

        assertThat(response.getBody()).satisfies(
                responseBody -> assertThat(responseBody.getFullName()).isEqualTo("User"),
                responseBody -> assertThat(responseBody.getEmail()).isEqualTo("user@email.com")
        );
        expectStatus(response, HttpStatus.OK);
    }

    @Test
    void testShouldDeleteSessionInformation() {
        sessionClient.createSession("user@email.com", "User password");
        ResponseEntity<Void> response = sessionClient.deleteSession();

        try {
            taskClient.getTasks();
            fail("An exception should have been thrown.");
        } catch (HttpClientErrorException exception) {
            expectStatus(exception, HttpStatus.FORBIDDEN);
        }

        expectStatus(response, HttpStatus.OK);
    }

    @TestConfiguration
    static class Configuration {

        public Configuration(InMemoryUserRepository userRepository, PasswordEncoder encoder) {
            this.userRepository = userRepository;
            this.encoder = encoder;
        }

        @PostConstruct
        public void setUpUserRepository() {
            userRepository.addUser(User.builder()
                    .email("user@email.com")
                    .password(encoder.encode("User password"))
                    .fullName("User").build()
            );
        }

        private final InMemoryUserRepository userRepository;
        private final PasswordEncoder encoder;
    }
}
