package com.taskify.taskboard.infra.configurations;

import com.taskify.taskboard.infra.RandomIdSequence;
import com.taskify.taskboard.infra.repositories.memory.InMemoryTaskRepository;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.taskify.taskboard.infra.configurations.Profiles.IN_MEMORY_PROFILE;

@Configuration
@Profile(IN_MEMORY_PROFILE)
public class InMemoryConfiguration {
    @Bean
    public InMemoryUserRepository getUserRepository() {
        return new InMemoryUserRepository();
    }

    @Bean
    public InMemoryTaskRepository getTaskRepository() {
        return new InMemoryTaskRepository();
    }

    @Bean
    public RandomIdSequence getIdSequence() {
        return new RandomIdSequence();
    }
}
