package com.taskify.taskboard.infra;

import com.taskify.taskboard.QueryParametersBuilder;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.clients.TaskClient;
import com.taskify.taskboard.infra.configurations.InMemoryConfiguration;
import com.taskify.taskboard.infra.configurations.NoSecurityConfiguration;
import com.taskify.taskboard.infra.configurations.TaskBoardConfiguration;
import com.taskify.taskboard.infra.controllers.TaskController;
import com.taskify.taskboard.infra.controllers.models.TaskMetadataModel;
import com.taskify.taskboard.infra.controllers.models.TaskModel;
import com.taskify.taskboard.infra.controllers.responses.CreateTaskResponse;
import com.taskify.taskboard.infra.controllers.responses.GetTaskResponse;
import com.taskify.taskboard.infra.controllers.responses.GetTasksResponse;
import com.taskify.taskboard.infra.repositories.memory.InMemoryTaskRepository;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.taskify.taskboard.ResponseParser.parseResponse;
import static com.taskify.taskboard.TestAssertions.expectStatus;
import static com.taskify.taskboard.infra.configurations.Profiles.IN_MEMORY_PROFILE;
import static com.taskify.taskboard.infra.configurations.Profiles.NO_SECURITY_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest
@ContextConfiguration(classes = {
        TaskController.class,
        TaskBoardConfiguration.class,
        NoSecurityConfiguration.class,
        InMemoryConfiguration.class,
        TaskControllerTest.Configuration.class,
})
@ActiveProfiles({IN_MEMORY_PROFILE, NO_SECURITY_PROFILE})
class TaskControllerTest {
    private static final String MOCK_USER = "User";
    private static final String MOCK_USER_EMAIL = "user@email.com";

    @Autowired
    private MockMvc mvc;
    private TaskClient taskClient;

    @Autowired
    private InMemoryTaskRepository taskRepository;

    @BeforeEach
    void setUp() {
        taskClient = TaskClient.from(mvc);
        taskRepository.clear();
    }

    @Test
    void testShouldGetSingleTask() throws Exception {
        Task task1 = Task.builder().title("Title 1").author("Author").fillDefaults();
        Task task2 = Task.builder().title("Title 2").author("Author").fillDefaults();
        Task task3 = Task.builder().title("Title 3").author("Author").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        MvcResult result1 = taskClient.getTask(task1.id());
        MvcResult result2 = taskClient.getTask(task2.id());
        MvcResult result3 = taskClient.getTask(task3.id());

        TaskModel taskModel1 = parseResponse(result1, GetTaskResponse.class).getTask();
        TaskModel taskModel2 = parseResponse(result2, GetTaskResponse.class).getTask();
        TaskModel taskModel3 = parseResponse(result3, GetTaskResponse.class).getTask();

        assertThat(taskModel1.toTask()).isEqualTo(task1);
        assertThat(taskModel2.toTask()).isEqualTo(task2);
        assertThat(taskModel3.toTask()).isEqualTo(task3);
        expectStatus(result1, HttpStatus.OK);
        expectStatus(result2, HttpStatus.OK);
        expectStatus(result3, HttpStatus.OK);
    }

    @Test
    void testShouldGetAllTasks() throws Exception {
        Task task1 = Task.builder().title("Title 1").author("Author").fillDefaults();
        Task task2 = Task.builder().title("Title 2").author("Author").fillDefaults();
        Task task3 = Task.builder().title("Title 3").author("Author").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        MvcResult result = taskClient.getTasks();
        List<TaskMetadataModel> tasks = parseResponse(result, GetTasksResponse.class).getTasks();

        assertThat(tasks).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task1),
                TaskMetadataModel.from(task2),
                TaskMetadataModel.from(task3)
        );
        expectStatus(result, HttpStatus.OK);
    }


    @Test
    void testShouldFilterTasks() throws Exception {
        Instant now = Instant.now();
        Instant later = now.plus(10, ChronoUnit.HOURS);

        Task task1 = Task.builder().title("Title 1").author("User 1").assignees("User 1").fillDefaults();
        Task task2 = Task.builder().title("Title 2").author("User 1").assignees("User 2", "User 3").fillDefaults();
        Task task3 = Task.builder().author("User").assignees("User").status(Task.Status.DONE).fillDefaults();
        Task task4 = Task.builder().author("User").assignees("User").due(now.minusSeconds(1)).fillDefaults();
        Task task5 = Task.builder().author("User").assignees("User").due(later.minusSeconds(1)).fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);
        taskRepository.addTask(task4);
        taskRepository.addTask(task5);

        MvcResult result1 = taskClient.getTasks(new QueryParametersBuilder()
                .parameter("assignees", "User 1")
                .parameter("assignees", "User 2")
                .build()
        );

        MvcResult result2 = taskClient.getTasks(new QueryParametersBuilder()
                .parameter("author", "User")
                .build()
        );

        MvcResult result3 = taskClient.getTasks(new QueryParametersBuilder()
                .parameter("author", "User")
                .parameter("status", "DONE")
                .build()
        );

        MvcResult result4 = taskClient.getTasks(new QueryParametersBuilder()
                .parameter("overdue", "OVERDUE")
                .build()
        );

        MvcResult result5 = taskClient.getTasks(new QueryParametersBuilder()
                .parameter("overdue", "NOT_OVERDUE")
                .parameter("time-left", Duration.between(now, later).toString())
                .build()
        );

        List<TaskMetadataModel> tasks1 = parseResponse(result1, GetTasksResponse.class).getTasks();
        List<TaskMetadataModel> tasks2 = parseResponse(result2, GetTasksResponse.class).getTasks();
        List<TaskMetadataModel> tasks3 = parseResponse(result3, GetTasksResponse.class).getTasks();
        List<TaskMetadataModel> tasks4 = parseResponse(result4, GetTasksResponse.class).getTasks();
        List<TaskMetadataModel> tasks5 = parseResponse(result5, GetTasksResponse.class).getTasks();

        assertThat(tasks1).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task1),
                TaskMetadataModel.from(task2)
        );

        assertThat(tasks2).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task3),
                TaskMetadataModel.from(task4),
                TaskMetadataModel.from(task5)
        );

        assertThat(tasks3).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task3)
        );

        assertThat(tasks4).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task4)
        );

        assertThat(tasks5).containsExactlyInAnyOrder(
                TaskMetadataModel.from(task1),
                TaskMetadataModel.from(task2),
                TaskMetadataModel.from(task3)
        );
        expectStatus(result1, HttpStatus.OK);
        expectStatus(result2, HttpStatus.OK);
        expectStatus(result3, HttpStatus.OK);
        expectStatus(result4, HttpStatus.OK);
        expectStatus(result5, HttpStatus.OK);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldCreateTasks() throws Exception {
        Task task1 = Task.builder().title("Title 1").assignees("Person 1").status(Task.Status.DONE).fillDefaults();
        Task task2 = Task.builder().title("Title 2").assignees("Person 2", "Person 1").fillDefaults();

        MvcResult result1 = taskClient.createTask(TaskModel.from(task1));
        MvcResult result2 = taskClient.createTask(TaskModel.from(task2));
        String taskId1 = parseResponse(result1, CreateTaskResponse.class).getTaskId();
        String taskId2 = parseResponse(result2, CreateTaskResponse.class).getTaskId();

        assertThat(taskRepository.getTaskById(taskId1)).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title 1"),
                createdTask -> assertThat(createdTask.assignees()).containsExactlyInAnyOrder("Person 1"),
                createdTask -> assertThat(createdTask.author()).isEqualTo(MOCK_USER),
                createdTask -> assertThat(createdTask.status()).isEqualTo(Task.Status.DONE)
        );
        assertThat(taskRepository.getTaskById(taskId2)).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title 2"),
                createdTask -> assertThat(createdTask.assignees()).containsExactlyInAnyOrder("Person 1", "Person 2"),
                createdTask -> assertThat(createdTask.author()).isEqualTo(MOCK_USER)
        );
        expectStatus(result1, HttpStatus.CREATED);
        expectStatus(result2, HttpStatus.CREATED);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldUpdateTask() throws Exception {
        Task task1 = Task.builder().title("Title 1").assignees("Person 2").status(Task.Status.DONE).fillDefaults();
        Task task2 = Task.builder().title("Title 2").status(Task.Status.IN_PROGRESS).fillDefaults();
        Task task3 = Task.builder().title("Title 2").fillDefaults();

        String taskId1 = parseResponse(taskClient.createTask(TaskModel.from(task1)), CreateTaskResponse.class).getTaskId();
        String taskId2 = parseResponse(taskClient.createTask(TaskModel.from(task2)), CreateTaskResponse.class).getTaskId();
        String taskId3 = parseResponse(taskClient.createTask(TaskModel.from(task3)), CreateTaskResponse.class).getTaskId();

        MvcResult result1 = taskClient.updateTask(taskId1, List.of("Person 1"));
        MvcResult result2 = taskClient.updateTask(taskId2, List.of("Person 1", "Person 2"), Task.Status.DONE);
        MvcResult result3 = taskClient.updateTask(taskId3, Task.Status.IN_PROGRESS);

        assertThat(taskRepository.getTaskById(taskId1)).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title 1"),
                createdTask -> assertThat(createdTask.assignees()).containsExactlyInAnyOrder("Person 1"),
                createdTask -> assertThat(createdTask.author()).isEqualTo(MOCK_USER),
                createdTask -> assertThat(createdTask.status()).isEqualTo(Task.Status.DONE)
        );
        assertThat(taskRepository.getTaskById(taskId2)).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title 2"),
                createdTask -> assertThat(createdTask.assignees()).containsExactlyInAnyOrder("Person 1", "Person 2"),
                createdTask -> assertThat(createdTask.author()).isEqualTo(MOCK_USER),
                createdTask -> assertThat(createdTask.status()).isEqualTo(Task.Status.DONE)
        );
        assertThat(taskRepository.getTaskById(taskId3)).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title 2"),
                createdTask -> assertThat(createdTask.assignees()).isEmpty(),
                createdTask -> assertThat(createdTask.author()).isEqualTo(MOCK_USER),
                createdTask -> assertThat(createdTask.status()).isEqualTo(Task.Status.IN_PROGRESS)
        );
        expectStatus(result1, HttpStatus.NO_CONTENT);
        expectStatus(result2, HttpStatus.NO_CONTENT);
        expectStatus(result3, HttpStatus.NO_CONTENT);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldUpdateTaskAsAssignee() throws Exception {
        Task task = Task.builder().title("Title").author("Person").assignees(MOCK_USER).fillDefaults();
        taskRepository.addTask(task);

        MvcResult result = taskClient.updateTask(task.id(), List.of("Person"));

        assertThat(taskRepository.getTaskById(task.id())).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title"),
                createdTask -> assertThat(createdTask.assignees()).containsExactlyInAnyOrder("Person"),
                createdTask -> assertThat(createdTask.author()).isEqualTo("Person")
        );
        expectStatus(result, HttpStatus.NO_CONTENT);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldNotUpdateTaskWithInvalidUser() throws Exception {
        Task task = Task.builder().title("Title").author("Person").status(Task.Status.DONE).fillDefaults();
        taskRepository.addTask(task);

        MvcResult result = taskClient.updateTask(task.id(), List.of(MOCK_USER), Task.Status.IN_PROGRESS);

        assertThat(taskRepository.getTaskById(task.id())).satisfies(
                createdTask -> assertThat(createdTask.title()).isEqualTo("Title"),
                createdTask -> assertThat(createdTask.assignees()).isEmpty(),
                createdTask -> assertThat(createdTask.author()).isEqualTo("Person"),
                createdTask -> assertThat(createdTask.status()).isEqualTo(Task.Status.DONE)
        );
        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldDeleteTask() throws Exception {
        Task task = Task.builder().title("Title").author(MOCK_USER).fillDefaults();
        taskRepository.addTask(task);

        assertThat(taskRepository.hasTask(task.id())).isTrue();

        MvcResult result = taskClient.deleteTask(task.id());

        assertThat(taskRepository.hasTask(task.id())).isFalse();
        expectStatus(result, HttpStatus.NO_CONTENT);
    }

    @Test
    @WithMockUser(username = MOCK_USER_EMAIL)
    void testShouldNotDeleteTaskWithInvalidUser() throws Exception {
        Task task = Task.builder().title("Title").author("Person").assignees(MOCK_USER).fillDefaults();
        taskRepository.addTask(task);

        assertThat(taskRepository.hasTask(task.id())).isTrue();

        MvcResult result = taskClient.deleteTask(task.id());

        assertThat(taskRepository.hasTask(task.id())).isTrue();
        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @TestConfiguration
    static class Configuration {

        public Configuration(InMemoryUserRepository userRepository) {
            this.userRepository = userRepository;
        }

        @PostConstruct
        private void setUpUserRepository() {
            userRepository.clear();
            userRepository.addUser(User.builder()
                    .email(MOCK_USER_EMAIL)
                    .fullName(MOCK_USER)
                    .password("pass")
                    .build());
        }

        private final InMemoryUserRepository userRepository;
    }
}
