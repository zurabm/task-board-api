package com.taskify.taskboard.infra.repositories;

import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.repositories.jpa.JpaUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@EntityScan(basePackages = "com.taskify.taskboard.infra")
@EnableJpaRepositories(basePackages = "com.taskify.taskboard.infra")
@ContextConfiguration(classes = JpaUserRepository.class)
@ActiveProfiles(PERSISTENCE_PROFILE)
class JpaUserRepositoryTest {
    @Autowired
    private JpaUserRepository userRepository;

    @Test
    void testShouldStoreUsers() {
        User user1 = User.builder().email("email1@email.com").password("Password 1").fullName("Test User 1").build();
        User user2 = User.builder().email("email2@email.com").password("Password 2").fullName("Test User 2").build();
        User user3 = User.builder().email("email3@email.com").password("Password 3").fullName("Test User 3").build();

        userRepository.addUser(user1);
        userRepository.addUser(user2);
        userRepository.addUser(user3);

        assertThat(userRepository.hasUserByEmail(user1.email())).isTrue();
        assertThat(userRepository.hasUserByEmail(user2.email())).isTrue();
        assertThat(userRepository.hasUserByEmail(user3.email())).isTrue();
        assertThat(userRepository.getUserByEmail(user1.email())).isEqualTo(user1);
        assertThat(userRepository.getUserByEmail(user2.email())).isEqualTo(user2);
        assertThat(userRepository.getUserByEmail(user3.email())).isEqualTo(user3);
    }

    @Test
    void testShouldRetrieveAllUsers() {
        User user1 = User.builder().email("email1@email.com").password("Password 1").fullName("Test User 1").build();
        User user2 = User.builder().email("email2@email.com").password("Password 2").fullName("Test User 2").build();
        User user3 = User.builder().email("email3@email.com").password("Password 3").fullName("Test User 3").build();

        userRepository.addUser(user1);
        userRepository.addUser(user2);
        userRepository.addUser(user3);

        assertThat(userRepository.getUsers()).containsExactlyInAnyOrder(user1, user2, user3);
    }

    @Test
    void testShouldNotFindNonExistentUser() {
        User user = User.builder().email("email@email.com").password("Password").fullName("User1").build();
        userRepository.addUser(user);

        assertThat(userRepository.hasUserByEmail("not.email@email.com")).isFalse();
    }
}
