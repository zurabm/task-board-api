package com.taskify.taskboard.infra.repositories;

import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.repositories.jpa.JpaTaskRepository;
import com.taskify.taskboard.infra.repositories.jpa.JpaUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@EntityScan(basePackages = "com.taskify.taskboard.infra")
@EnableJpaRepositories(basePackages = "com.taskify.taskboard.infra")
@ContextConfiguration(classes = {JpaTaskRepository.class, JpaUserRepository.class})
@ActiveProfiles(PERSISTENCE_PROFILE)
public class JpaTaskRepositoryTest {
    @Autowired
    private JpaTaskRepository taskRepository;

    @Autowired
    private JpaUserRepository userRepository;

    private Instant now;

    @PostConstruct
    private void setUpUserRepository() {
        userRepository.addUser(User.builder().email("some1@mail.com").password("pass").fullName("User").build());
        userRepository.addUser(User.builder().email("some2@mail.com").password("pass").fullName("Author").build());
        userRepository.addUser(User.builder().email("some3@mail.com").password("pass").fullName("Assignee 1").build());
        userRepository.addUser(User.builder().email("some4@mail.com").password("pass").fullName("Freeloader").build());
    }

    @BeforeEach
    void setUp() {
        now = Instant.now();
    }

    @Test
    void testShouldCreateTasks() {
        Task task1 = Task.builder().title("Task 1").author("Author").due(Instant.now()).fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("Author").assignees("Assignee 1").fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("Author").id("Custom ID").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        assertThat(taskRepository.hasTaskById(task1.id())).isTrue();
        assertThat(taskRepository.hasTaskById(task2.id())).isTrue();
        assertThat(taskRepository.hasTaskById(task3.id())).isTrue();
        assertThat(taskRepository.getTaskById(task1.id())).isEqualTo(task1);
        assertThat(taskRepository.getTaskById(task2.id())).isEqualTo(task2);
        assertThat(taskRepository.getTaskById(task3.id())).isEqualTo(task3);
    }

    @Test
    void testShouldUpdateTasks() {
        Task initialTask1 = Task.builder().title("Task 1").author("Author").due(Instant.now()).fillDefaults();
        Task initialTask2 = Task.builder().title("Task 2").author("Author").assignees("Assignee 1").fillDefaults();
        Task initialTask3 = Task.builder().title("Task 3").author("Author").id("Custom ID").fillDefaults();

        taskRepository.addTask(initialTask1);
        taskRepository.addTask(initialTask2);
        taskRepository.addTask(initialTask3);

        Task updatedTask1 = initialTask1.toBuilder().title("Task 4").status(Task.Status.DONE).build();
        Task updatedTask2 = initialTask2.toBuilder().author("Assignee 1").assignees("Author").build();
        Task updatedTask3 = initialTask3.toBuilder().title("Task 6").assignees("Author", "Assignee 1").build();

        taskRepository.updateTask(initialTask1.id(), updatedTask1);
        taskRepository.updateTask(initialTask2.id(), updatedTask2);
        taskRepository.updateTask(initialTask3.id(), updatedTask3);

        assertThat(taskRepository.hasTaskById(initialTask1.id())).isTrue();
        assertThat(taskRepository.hasTaskById(initialTask2.id())).isTrue();
        assertThat(taskRepository.hasTaskById(initialTask3.id())).isTrue();
        assertThat(taskRepository.getTaskById(initialTask1.id())).isEqualTo(updatedTask1);
        assertThat(taskRepository.getTaskById(initialTask2.id())).isEqualTo(updatedTask2);
        assertThat(taskRepository.getTaskById(initialTask3.id())).isEqualTo(updatedTask3);
    }

    @Test
    void testShouldFilterTasksByAssignees() {
        Task task1 = Task.builder().author("Author").assignees("Assignee 1").fillDefaults();
        Task task2 = Task.builder().author("Author").assignees("Assignee 1", "User").fillDefaults();
        Task task3 = Task.builder().author("Author").assignees("Author", "Assignee 1").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        TaskFilter filter1 = TaskFilter.builder().build();
        TaskFilter filter2 = TaskFilter.builder().assignees("Assignee 1").build();
        TaskFilter filter3 = TaskFilter.builder().assignees("Assignee 1", "User").build();
        TaskFilter filter4 = TaskFilter.builder().assignees("User").build();
        TaskFilter filter5 = TaskFilter.builder().assignees("Author").build();
        TaskFilter filter6 = TaskFilter.builder().assignees("Freeloader").build();

        assertThat(taskRepository.getTasksByFilter(filter1, now)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskRepository.getTasksByFilter(filter2, now)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskRepository.getTasksByFilter(filter3, now)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskRepository.getTasksByFilter(filter4, now)).containsExactlyInAnyOrder(task2);
        assertThat(taskRepository.getTasksByFilter(filter5, now)).containsExactlyInAnyOrder(task3);
        assertThat(taskRepository.getTasksByFilter(filter6, now)).containsExactlyInAnyOrder();
    }

    @Test
    void testShouldFilterTasksByAuthor() {
        Task task1 = Task.builder().author("Author").assignees("Assignee 1").fillDefaults();
        Task task2 = Task.builder().author("Assignee 1").fillDefaults();
        Task task3 = Task.builder().author("Author").assignees("Author", "Assignee 1").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        TaskFilter filter1 = TaskFilter.builder().build();
        TaskFilter filter2 = TaskFilter.builder().author("Assignee 1").build();
        TaskFilter filter3 = TaskFilter.builder().author("Author").build();
        TaskFilter filter4 = TaskFilter.builder().author("Freeloader").build();

        assertThat(taskRepository.getTasksByFilter(filter1, now)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskRepository.getTasksByFilter(filter2, now)).containsExactlyInAnyOrder(task2);
        assertThat(taskRepository.getTasksByFilter(filter3, now)).containsExactlyInAnyOrder(task1, task3);
        assertThat(taskRepository.getTasksByFilter(filter4, now)).containsExactlyInAnyOrder();
    }

    @Test
    void testShouldFilterTasksByStatus() {
        Task task1 = Task.builder().author("Author").status(Task.Status.NOT_DONE).fillDefaults();
        Task task2 = Task.builder().author("Author").status(Task.Status.IN_PROGRESS).fillDefaults();
        Task task3 = Task.builder().author("Author").status(Task.Status.NOT_DONE).fillDefaults();
        Task task4 = Task.builder().author("Author").status(Task.Status.NOT_DONE).fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);
        taskRepository.addTask(task4);

        TaskFilter filter1 = TaskFilter.builder().build();
        TaskFilter filter2 = TaskFilter.builder().status(Task.Status.NOT_DONE).build();
        TaskFilter filter3 = TaskFilter.builder().status(Task.Status.IN_PROGRESS).build();
        TaskFilter filter4 = TaskFilter.builder().status(Task.Status.DONE).build();

        assertThat(taskRepository.getTasksByFilter(filter1, now)).containsExactlyInAnyOrder(task1, task2, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter2, now)).containsExactlyInAnyOrder(task1, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter3, now)).containsExactlyInAnyOrder(task2);
        assertThat(taskRepository.getTasksByFilter(filter4, now)).containsExactlyInAnyOrder();
    }

    @Test
    void testShouldFilterTasksByOverdue() {
        Instant near = now.plus(10, ChronoUnit.MINUTES);
        Instant later = now.plus(10, ChronoUnit.DAYS);

        Duration tillNow = Duration.between(now, now);
        Duration tillNear = Duration.between(now, near);
        Duration tillLater = Duration.between(now, later);

        Task task1 = Task.builder().author("Author").due(now.minusSeconds(1)).fillDefaults();
        Task task2 = Task.builder().author("Author").due(near.minusSeconds(1)).fillDefaults();
        Task task3 = Task.builder().author("Author").due(later.minusSeconds(1)).fillDefaults();
        Task task4 = Task.builder().author("Author").due(later.minusSeconds(1)).fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);
        taskRepository.addTask(task4);

        TaskFilter filter1 = TaskFilter.builder().build();
        TaskFilter filter2 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).build();
        TaskFilter filter3 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).timeLeft(tillNow).build();
        TaskFilter filter4 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).timeLeft(tillNear).build();
        TaskFilter filter5 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).timeLeft(tillLater).build();
        TaskFilter filter6 = TaskFilter.builder().overdue(TaskFilter.Overdue.NOT_OVERDUE).timeLeft(tillNow).build();
        TaskFilter filter7 = TaskFilter.builder().overdue(TaskFilter.Overdue.NOT_OVERDUE).timeLeft(tillNear).build();
        TaskFilter filter8 = TaskFilter.builder().overdue(TaskFilter.Overdue.NOT_OVERDUE).timeLeft(tillLater).build();

        assertThat(taskRepository.getTasksByFilter(filter1, now)).containsExactlyInAnyOrder(task1, task2, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter2, now)).containsExactlyInAnyOrder(task1);
        assertThat(taskRepository.getTasksByFilter(filter3, now)).containsExactlyInAnyOrder(task1);
        assertThat(taskRepository.getTasksByFilter(filter4, now)).containsExactlyInAnyOrder(task1, task2);
        assertThat(taskRepository.getTasksByFilter(filter5, now)).containsExactlyInAnyOrder(task1, task2, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter6, now)).containsExactlyInAnyOrder(task2, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter7, now)).containsExactlyInAnyOrder(task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter8, now)).containsExactlyInAnyOrder();
    }

    @Test
    void testShouldFilterTasksByMultipleCriteria() {
        Instant near = now.plus(10, ChronoUnit.MINUTES);
        Instant later = now.plus(10, ChronoUnit.DAYS);

        Task task1 = Task.builder().author("Author").assignees("Assignee 1").due(now.minusSeconds(1)).fillDefaults();
        Task task2 = Task.builder().author("Assignee 1").status(Task.Status.IN_PROGRESS).due(near.minusSeconds(1)).fillDefaults();
        Task task3 = Task.builder().author("User").status(Task.Status.NOT_DONE).due(later.minusSeconds(1)).fillDefaults();
        Task task4 = Task.builder().author("Author").status(Task.Status.NOT_DONE).due(later.minusSeconds(1)).fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);
        taskRepository.addTask(task4);

        TaskFilter filter1 = TaskFilter.builder()
                .build();

        TaskFilter filter2 = TaskFilter.builder()
                .assignees("Assignee 1")
                .overdue(TaskFilter.Overdue.NOT_OVERDUE)
                .build();

        TaskFilter filter3 = TaskFilter.builder()
                .assignees("User", "Author")
                .author("Author")
                .status(Task.Status.IN_PROGRESS)
                .overdue(TaskFilter.Overdue.OVERDUE)
                .build();

        TaskFilter filter4 = TaskFilter.builder()
                .author("User")
                .status(Task.Status.NOT_DONE)
                .build();

        assertThat(taskRepository.getTasksByFilter(filter1, now)).containsExactlyInAnyOrder(task1, task2, task3, task4);
        assertThat(taskRepository.getTasksByFilter(filter2, now)).containsExactlyInAnyOrder();
        assertThat(taskRepository.getTasksByFilter(filter3, now)).containsExactlyInAnyOrder();
        assertThat(taskRepository.getTasksByFilter(filter4, now)).containsExactlyInAnyOrder(task3);
    }

    @Test
    void testShouldRemoveTasks() {
        Task task1 = Task.builder().title("T1").author("User").fillDefaults();
        Task task2 = Task.builder().title("T1").author("Author").assignees("Assignee 1").fillDefaults();
        Task task3 = Task.builder().title("T1").author("Author").id("Custom ID").fillDefaults();

        taskRepository.addTask(task1);
        taskRepository.addTask(task2);
        taskRepository.addTask(task3);

        taskRepository.removeTask(task1.id());
        taskRepository.removeTask(task2.id());
        taskRepository.removeTask(task3.id());

        assertThat(taskRepository.hasTaskById(task1.id())).isFalse();
        assertThat(taskRepository.hasTaskById(task2.id())).isFalse();
        assertThat(taskRepository.hasTaskById(task3.id())).isFalse();
    }
}
