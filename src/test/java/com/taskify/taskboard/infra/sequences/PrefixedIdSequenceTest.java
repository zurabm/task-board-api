package com.taskify.taskboard.infra.sequences;

import com.taskify.taskboard.infra.PrefixedIdSequence;
import com.taskify.taskboard.infra.repositories.jpa.JpaSettingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@EntityScan(basePackages = "com.taskify.taskboard.infra")
@EnableJpaRepositories(basePackages = "com.taskify.taskboard.infra")
@ContextConfiguration(classes = {JpaSettingRepository.class})
@ActiveProfiles(PERSISTENCE_PROFILE)
class PrefixedIdSequenceTest {
    @Autowired
    private JpaSettingRepository settingRepository;
    private PrefixedIdSequence idSequence;

    @Test
    void testShouldGenerateUniqueIds() {
        idSequence = PrefixedIdSequence.withPrefix("", settingRepository);
        List<String> ids = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            ids.add(idSequence.next());
        }

        assertThat(ids).containsExactly("1", "2", "3", "4", "5");
    }

    @Test
    void testShouldAppendPrefixToIds() {
        idSequence = PrefixedIdSequence.withPrefix("Test-", settingRepository);

        assertThat(idSequence.next()).isEqualTo("Test-1");
    }

    @Test
    void testShouldPersistIds() {
        idSequence = PrefixedIdSequence.withPrefix("", settingRepository);
        idSequence.next();

        idSequence = PrefixedIdSequence.withPrefix("", settingRepository);
        assertThat(idSequence.next()).isEqualTo("2");
    }
}
