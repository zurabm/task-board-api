package com.taskify.taskboard.infra;

import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.clients.SessionClient;
import com.taskify.taskboard.infra.clients.TaskClient;
import com.taskify.taskboard.infra.clients.UserClient;
import com.taskify.taskboard.infra.configurations.InMemoryConfiguration;
import com.taskify.taskboard.infra.configurations.SecurityConfiguration;
import com.taskify.taskboard.infra.configurations.TaskBoardConfiguration;
import com.taskify.taskboard.infra.controllers.SessionController;
import com.taskify.taskboard.infra.controllers.TaskController;
import com.taskify.taskboard.infra.controllers.UserController;
import com.taskify.taskboard.infra.controllers.models.TaskModel;
import com.taskify.taskboard.infra.repositories.memory.InMemoryTaskRepository;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.taskify.taskboard.TestAssertions.expectStatus;
import static com.taskify.taskboard.infra.configurations.Profiles.IN_MEMORY_PROFILE;
import static com.taskify.taskboard.infra.configurations.Profiles.SECURITY_PROFILE;

@WebMvcTest
@ContextConfiguration(classes = {
        SecurityConfiguration.class,
        TaskBoardUserDetailsService.class,
        TaskBoardConfiguration.class,
        InMemoryConfiguration.class,
        AuthorizationTest.Configuration.class,
        UserController.class,
        SessionController.class,
        TaskController.class,
})
@ActiveProfiles({IN_MEMORY_PROFILE, SECURITY_PROFILE})
public class AuthorizationTest {
    @Autowired
    private MockMvc mvc;
    private UserClient userClient;
    private SessionClient sessionClient;
    private TaskClient taskClient;

    @Autowired
    private InMemoryTaskRepository taskRepository;

    @BeforeEach
    void setUp() {
        userClient = UserClient.from(mvc);
        sessionClient = SessionClient.from(mvc);
        taskClient = TaskClient.from(mvc);
    }

    @Test
    void testShouldRegisterUserWithoutAuthentication() throws Exception {
        User user = User.builder().email("new@email.com").password("with pass").fullName("Some user").build();
        MvcResult result = userClient.registerUser(user.email(), user.password(), user.fullName());

        expectStatus(result, HttpStatus.CREATED);
    }

    @Test
    void testShouldCreateSessionWithoutAuthentication() throws Exception {
        MvcResult result = sessionClient.createSession("user@email.com", "User password");

        expectStatus(result, HttpStatus.CREATED);
    }

    @Test
    void testShouldNotGetSessionInformationWithoutAuthentication() throws Exception {
        MvcResult result = sessionClient.getSession();

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldGetSessionInformationWithAuthentication() throws Exception {
        MvcResult result = sessionClient.getSession();

        expectStatus(result, HttpStatus.OK);
    }

    @Test
    void testShouldNotDeleteSessionInformationWithoutAuthentication() throws Exception {
        MvcResult result = sessionClient.deleteSession();

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldDeleteSessionInformationWithAuthentication() throws Exception {
        MvcResult result = sessionClient.deleteSession();

        expectStatus(result, HttpStatus.OK);
    }

    @Test
    void testShouldNotGetUsersWithoutAuthentication() throws Exception {
        MvcResult result = userClient.getUsers();

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldGetUsersWithAuthentication() throws Exception {
        MvcResult result = userClient.getUsers();

        expectStatus(result, HttpStatus.OK);
    }

    @Test
    void testShouldNotGetTaskListWithoutAuthentication() throws Exception {
        MvcResult result = taskClient.getTasks();

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldGetTaskListWithAuthentication() throws Exception {
        MvcResult result = taskClient.getTasks();

        expectStatus(result, HttpStatus.OK);
    }

    @Test
    void testShouldNotGetSingleTaskWithoutAuthentication() throws Exception {
        MvcResult result = taskClient.getTask("ID");

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldGetSingleTaskWithAuthentication() throws Exception {
        MvcResult result = taskClient.getTask("ID");

        expectStatus(result, HttpStatus.OK);
    }

    @Test
    void testShouldNotCreateTaskWithoutAuthentication() throws Exception {
        MvcResult result = taskClient.createTask(TaskModel.from(Task.builder().fillDefaults()));

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldCreateTaskWithAuthentication() throws Exception {
        MvcResult result = taskClient.createTask(TaskModel.from(Task.builder().fillDefaults()));

        expectStatus(result, HttpStatus.CREATED);
    }

    @Test
    void testShouldNotUpdateTaskWithoutAuthentication() throws Exception {
        MvcResult result = taskClient.updateTask("ID", List.of("New Assignee"), Task.Status.DONE);

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldUpdateTaskWithAuthentication() throws Exception {
        MvcResult result = taskClient.updateTask("ID", List.of("New Assignee"), Task.Status.DONE);

        expectStatus(result, HttpStatus.NO_CONTENT);
    }

    @Test
    void testShouldNotDeleteTaskWithoutAuthentication() throws Exception {
        MvcResult result = taskClient.deleteTask("ID");

        expectStatus(result, HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("user@email.com")
    void testShouldDeleteTaskWithAuthentication() throws Exception {
        taskRepository.addTask(Task.builder().id("Test ID").author("User").fillDefaults());
        MvcResult result = taskClient.deleteTask("Test ID");

        expectStatus(result, HttpStatus.NO_CONTENT);
    }

    @TestConfiguration
    static class Configuration {

        public Configuration(InMemoryUserRepository userRepository,
                             InMemoryTaskRepository taskRepository,
                             PasswordEncoder encoder) {
            this.userRepository = userRepository;
            this.taskRepository = taskRepository;
            this.encoder = encoder;
        }

        @PostConstruct
        public void setUpUserRepository() {
            userRepository.addUser(User.builder()
                    .email("user@email.com")
                    .password(encoder.encode("User password"))
                    .fullName("User").build()
            );
        }

        @PostConstruct
        public void setupTaskRepository() {
            taskRepository.addTask(Task.builder().id("ID").author("User").fillDefaults());
        }

        private final InMemoryUserRepository userRepository;
        private final InMemoryTaskRepository taskRepository;
        private final PasswordEncoder encoder;
    }
}
