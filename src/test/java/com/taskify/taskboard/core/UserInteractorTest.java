package com.taskify.taskboard.core;

import com.taskify.taskboard.core.users.InvalidEmailException;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.core.users.UserInteractor;
import com.taskify.taskboard.infra.repositories.memory.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class UserInteractorTest {
    private UserInteractor userInteractor;

    @BeforeEach
    void setUp() {
        userInteractor = new UserInteractor(new InMemoryUserRepository());
    }

    @Test
    void testShouldRegisterUser() {
        userInteractor.registerUser(User.builder().email("user@email.com").password("Password").fullName("User").build());
    }

    @Test
    void testShouldRegisterMultipleUsers() {
        userInteractor.registerUser(User.builder().email("user1@email.com").password("Password 1").fullName("User 1").build());
        userInteractor.registerUser(User.builder().email("user2@email.com").password("Password 2").fullName("User 2").build());
        userInteractor.registerUser(User.builder().email("user3@email.com").password("Password 3").fullName("User 3").build());
    }

    @Test
    void testShouldRetrieveUsers() {
        User user1 = User.builder().email("user1@email.com").password("Password").fullName("Test 1").build();
        User user2 = User.builder().email("user2@email.com").password("Password").fullName("Test 2").build();
        User user3 = User.builder().email("user3@email.com").password("Password").fullName("Test 3").build();

        userInteractor.registerUser(user1);
        userInteractor.registerUser(user2);
        userInteractor.registerUser(user3);

        assertThat(userInteractor.getUserByEmail(user1.email())).isEqualTo(user1);
        assertThat(userInteractor.getUserByEmail(user2.email())).isEqualTo(user2);
        assertThat(userInteractor.getUserByEmail(user3.email())).isEqualTo(user3);
    }

    @Test
    void testShouldRetrieveAllUserS() {
        User user1 = User.builder().email("user1@email.com").password("Password").fullName("Test 1").build();
        User user2 = User.builder().email("user2@email.com").password("Password").fullName("Test 2").build();
        User user3 = User.builder().email("user3@email.com").password("Password").fullName("Test 3").build();

        userInteractor.registerUser(user1);
        userInteractor.registerUser(user2);
        userInteractor.registerUser(user3);

        assertThat(userInteractor.getUsers()).containsExactlyInAnyOrder(user1, user2, user3);
    }

    @Test
    void testShouldNotRetrieveNonExistentUser() {
        userInteractor.registerUser(User.builder().email("user@email.com").password("Password").fullName("User").build());

        assertThatExceptionOfType(InvalidEmailException.class)
                .isThrownBy(() -> userInteractor.getUserByEmail("not.user@email.com"));
    }
}
