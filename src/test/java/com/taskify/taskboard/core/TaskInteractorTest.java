package com.taskify.taskboard.core;

import com.taskify.taskboard.core.tasks.InvalidTaskUpdateException;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;
import com.taskify.taskboard.core.tasks.TaskInteractor;
import com.taskify.taskboard.infra.RandomIdSequence;
import com.taskify.taskboard.infra.repositories.memory.InMemoryTaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class TaskInteractorTest {
    private TaskInteractor taskInteractor;

    @BeforeEach
    void setUp() {
        taskInteractor = new TaskInteractor(new InMemoryTaskRepository(), new RandomIdSequence());
    }

    @Test
    void testShouldCreateUniqueTaskIds() {
        String taskId1 = taskInteractor.createTaskId();
        String taskId2 = taskInteractor.createTaskId();
        String taskId3 = taskInteractor.createTaskId();

        assertThat(taskId1).isNotNull();
        assertThat(taskId2).isNotNull();
        assertThat(taskId3).isNotNull();

        assertThat(taskId1).isNotEqualTo(taskId2).isNotEqualTo(taskId3);
        assertThat(taskId2).isNotEqualTo(taskId3);
    }

    @Test
    void testShouldCreateNewTask() {
        Task task = Task.builder().title("Task").description("Description").author("User").fillDefaults();
        taskInteractor.createTask(task);

        assertThat(taskInteractor.getTaskById(task.id())).isEqualTo(task);
    }

    @Test
    void testShouldCreateTaskForMultipleUsers() {
        Task task1 = Task.builder().title("Task 1").description("None").author("User 1").fillDefaults();
        Task task2 = Task.builder().title("Task 2").description("None").author("User 2").fillDefaults();
        Task task3 = Task.builder().title("Task 3").description("None").author("User 3").fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        assertThat(taskInteractor.getTaskById(task1.id())).isEqualTo(task1);
        assertThat(taskInteractor.getTaskById(task2.id())).isEqualTo(task2);
        assertThat(taskInteractor.getTaskById(task3.id())).isEqualTo(task3);
    }

    @Test
    void testShouldCreateMultipleTasksForUser() {
        Task task1 = Task.builder().title("Task 1").description("None").author("User").fillDefaults();
        Task task2 = Task.builder().title("Task 2").description("None").author("User").fillDefaults();
        Task task3 = Task.builder().title("Task 3").description("None").author("User").fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        assertThat(taskInteractor.getTaskById(task1.id())).isEqualTo(task1);
        assertThat(taskInteractor.getTaskById(task2.id())).isEqualTo(task2);
        assertThat(taskInteractor.getTaskById(task3.id())).isEqualTo(task3);
    }

    @Test
    void testShouldCreateTasksWithAssignees() {
        Task task1 = Task.builder().title("Do 1").description("None").author("User 1").assignees("User 2").fillDefaults();
        Task task2 = Task.builder().title("Do 2").description("None").author("User 2").assignees("User 2").fillDefaults();
        Task task3 = Task.builder().title("Do 3").description("None").author("User 3").assignees("User 2", "User 1").fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        assertThat(taskInteractor.getTaskById(task1.id())).isEqualTo(task1);
        assertThat(taskInteractor.getTaskById(task2.id())).isEqualTo(task2);
        assertThat(taskInteractor.getTaskById(task3.id())).isEqualTo(task3);
    }

    @Test
    void testShouldUpdateTaskStatus() {
        Task initialTask = Task.builder().title("Task").author("Author").status(Task.Status.NOT_DONE).fillDefaults();
        Task updatedTask1 = initialTask.toBuilder().status(Task.Status.IN_PROGRESS).build();
        Task updatedTask2 = initialTask.toBuilder().status(Task.Status.DONE).build();
        taskInteractor.createTask(initialTask);

        taskInteractor.updateTaskStatus("Author", initialTask.id(), updatedTask1.status());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask1);

        taskInteractor.updateTaskStatus("Author", initialTask.id(), updatedTask2.status());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask2);
    }

    @Test
    void testShouldUpdateTaskStatusWithAuthorAsAssignee() {
        Task initialTask = Task.builder()
                .title("Task")
                .author("Author")
                .assignees("Author")
                .status(Task.Status.NOT_DONE)
                .fillDefaults();

        Task updatedTask = initialTask.toBuilder().status(Task.Status.IN_PROGRESS).build();

        taskInteractor.createTask(initialTask);
        taskInteractor.updateTaskStatus("Author", initialTask.id(), updatedTask.status());

        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask);
    }

    @Test
    void testShouldUpdateTaskStatusAsAssignee() {
        Task initialTask = Task.builder()
                .title("Task")
                .author("Author")
                .assignees("Assignee 1", "Assignee 2")
                .status(Task.Status.NOT_DONE)
                .fillDefaults();

        Task updatedTask1 = initialTask.toBuilder().status(Task.Status.DONE).build();
        Task updatedTask2 = initialTask.toBuilder().status(Task.Status.IN_PROGRESS).build();

        taskInteractor.createTask(initialTask);
        taskInteractor.updateTaskStatus("Assignee 1", initialTask.id(), updatedTask1.status());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask1);
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask1);

        taskInteractor.updateTaskStatus("Assignee 2", initialTask.id(), updatedTask2.status());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask2);
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask2);
    }

    @Test
    void testShouldNotUpdateTaskStatusAsInvalidUser() {
        Task task = Task.builder().title("Task").author("Author").assignees("Assignee 1", "Assignee 2").fillDefaults();
        taskInteractor.createTask(task);

        assertThatExceptionOfType(InvalidTaskUpdateException.class)
                .isThrownBy(() -> taskInteractor.updateTaskStatus("User", task.id(), Task.Status.DONE));
    }

    @Test
    void testShouldUpdateTaskAssignees() {
        Task initialTask = Task.builder().title("Task").author("Author").assignees("Assignee 1", "Assignee 2").fillDefaults();
        Task updatedTask1 = initialTask.toBuilder().assignees("Assignee 1", "Assignee 3").build();
        Task updatedTask2 = initialTask.toBuilder().assignees().build();
        taskInteractor.createTask(initialTask);

        taskInteractor.updateTaskAssignees("Author", initialTask.id(), updatedTask1.assignees());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask1);

        taskInteractor.updateTaskAssignees("Author", initialTask.id(), updatedTask2.assignees());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask2);
    }

    @Test
    void testShouldUpdateTaskAssigneesAsAssignee() {
        Task initialTask = Task.builder().title("Task").author("Author").assignees("Assignee 1", "Assignee 2").fillDefaults();
        Task updatedTask1 = initialTask.toBuilder().assignees("Author", "Assignee 3").build();
        Task updatedTask2 = initialTask.toBuilder().assignees().build();
        taskInteractor.createTask(initialTask);

        taskInteractor.updateTaskAssignees("Assignee 1", initialTask.id(), updatedTask1.assignees());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask1);

        taskInteractor.updateTaskAssignees("Assignee 3", initialTask.id(), updatedTask2.assignees());
        assertThat(taskInteractor.getTaskById(initialTask.id())).isEqualTo(updatedTask2);
    }

    @Test
    void testShouldNotUpdateTaskAssigneesAsInvalidUser() {
        Task task = Task.builder().title("Task").author("Author").assignees("Assignee 1", "Assignee 2").fillDefaults();
        taskInteractor.createTask(task);

        assertThatExceptionOfType(InvalidTaskUpdateException.class)
                .isThrownBy(() -> taskInteractor.updateTaskAssignees("User", task.id(), Set.of()));
    }

    @Test
    void testShouldFilterTasksByAssignees() {
        Task task1 = Task.builder().title("Task 1").author("User 1").assignees("User 1", "User 2").fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 1").assignees("User 3").fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").assignees("User 2").fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder().assignees("User 1").build();
        TaskFilter filter2 = TaskFilter.builder().assignees("User 2").build();
        TaskFilter filter3 = TaskFilter.builder().assignees("User 3").build();
        TaskFilter filter4 = TaskFilter.builder().assignees("User 1", "User 2", "User 3").build();
        TaskFilter filter5 = TaskFilter.builder().build();
        TaskFilter filter6 = TaskFilter.builder().assignees("User 4").build();
        TaskFilter filter7 = TaskFilter.builder().assignees().build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).containsExactlyInAnyOrder(task1);
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task1, task3);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task2);
        assertThat(taskInteractor.getTasksByFilter(filter4)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskInteractor.getTasksByFilter(filter5)).containsExactlyInAnyOrder(task1, task2, task3);
        assertThat(taskInteractor.getTasksByFilter(filter6)).isEmpty();
        assertThat(taskInteractor.getTasksByFilter(filter7)).isEmpty();
    }


    @Test
    void testShouldFilterTasksByAuthor() {
        Task task1 = Task.builder().title("Task 1").author("User 1").fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 2").fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder().author("User 1").build();
        TaskFilter filter2 = TaskFilter.builder().author("User 2").build();
        TaskFilter filter3 = TaskFilter.builder().build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).containsExactlyInAnyOrder(task1, task3);
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task2);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task1, task2, task3);
    }

    @Test
    void testShouldFilterTasksByStatus() {
        Task task1 = Task.builder().title("Task 1").author("User 1").status(Task.Status.DONE).fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 1").status(Task.Status.IN_PROGRESS).fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").status(Task.Status.DONE).fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder().status(Task.Status.NOT_DONE).build();
        TaskFilter filter2 = TaskFilter.builder().status(Task.Status.IN_PROGRESS).build();
        TaskFilter filter3 = TaskFilter.builder().status(Task.Status.DONE).build();
        TaskFilter filter4 = TaskFilter.builder().build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).isEmpty();
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task2);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task1, task3);
        assertThat(taskInteractor.getTasksByFilter(filter4)).containsExactlyInAnyOrder(task1, task2, task3);
    }

    @Test
    void testShouldFilterTasksByOverdue() {
        Instant now = Instant.now();
        Instant later = now.plus(10, ChronoUnit.MINUTES);

        Task task1 = Task.builder().title("Task 1").author("User 1").due(now).fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 1").due(now).fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").due(later).fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).build();
        TaskFilter filter2 = TaskFilter.builder().overdue(TaskFilter.Overdue.NOT_OVERDUE).build();
        TaskFilter filter3 = TaskFilter.builder().build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).containsExactlyInAnyOrder(task1, task2);
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task3);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task1, task2, task3);
    }

    @Test
    void testShouldFilterTasksByOverdueWithTimeLeftSpecified() {
        Instant now = Instant.now();
        Instant near = now.plus(10, ChronoUnit.MINUTES);
        Instant later = now.plus(10, ChronoUnit.HOURS);

        Duration tillNear = Duration.between(now, near);

        Task task1 = Task.builder().title("Task 1").author("User 1").due(now).fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 1").due(near).fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").due(later).fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder().overdue(TaskFilter.Overdue.OVERDUE).timeLeft(tillNear).build();
        TaskFilter filter2 = TaskFilter.builder().overdue(TaskFilter.Overdue.NOT_OVERDUE).timeLeft(tillNear).build();
        TaskFilter filter3 = TaskFilter.builder().build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).containsExactlyInAnyOrder(task1, task2);
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task3);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task1, task2, task3);
    }

    @Test
    void testShouldFilterTasksByMultipleConditions() {
        Instant now = Instant.now();
        Instant later = now.plus(10, ChronoUnit.MINUTES);

        Task task1 = Task.builder().title("Task 1").author("User 1").assignees("User 1").due(now)
                .status(Task.Status.DONE).fillDefaults();

        Task task2 = Task.builder().title("Task 2").author("User 1").assignees("User 2").due(now)
                .status(Task.Status.IN_PROGRESS).fillDefaults();

        Task task3 = Task.builder().title("Task 3").author("User 1").assignees("User 1", "User 3").due(later)
                .status(Task.Status.NOT_DONE).fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        TaskFilter filter1 = TaskFilter.builder()
                .assignees("User 1")
                .overdue(TaskFilter.Overdue.NOT_OVERDUE)
                .build();

        TaskFilter filter2 = TaskFilter.builder()
                .assignees("User 1", "User 2")
                .status(Task.Status.IN_PROGRESS)
                .overdue(TaskFilter.Overdue.OVERDUE)
                .build();

        TaskFilter filter3 = TaskFilter.builder()
                .assignees("User 1", "User 2")
                .author("User 1")
                .status(Task.Status.IN_PROGRESS)
                .overdue(TaskFilter.Overdue.OVERDUE)
                .build();

        TaskFilter filter4 = TaskFilter.builder()
                .build();

        assertThat(taskInteractor.getTasksByFilter(filter1)).containsExactlyInAnyOrder(task3);
        assertThat(taskInteractor.getTasksByFilter(filter2)).containsExactlyInAnyOrder(task2);
        assertThat(taskInteractor.getTasksByFilter(filter3)).containsExactlyInAnyOrder(task2);
        assertThat(taskInteractor.getTasksByFilter(filter4)).containsExactlyInAnyOrder(task1, task2, task3);
    }

    @Test
    void testShouldDeleteTasks() {
        Instant now = Instant.now();

        Task task1 = Task.builder().title("Task 1").author("User 1").assignees("User 1", "User 2").fillDefaults();
        Task task2 = Task.builder().title("Task 2").author("User 1").assignees("User 3").due(now).fillDefaults();
        Task task3 = Task.builder().title("Task 3").author("User 1").assignees("User 2").due(now).fillDefaults();

        taskInteractor.createTask(task1);
        taskInteractor.createTask(task2);
        taskInteractor.createTask(task3);

        taskInteractor.deleteTask("User 1", task1.id());
        assertThat(taskInteractor.getTasksByFilter(TaskFilter.empty())).containsExactlyInAnyOrder(task2, task3);

        taskInteractor.deleteTask("User 1", task2.id());
        assertThat(taskInteractor.getTasksByFilter(TaskFilter.empty())).containsExactlyInAnyOrder(task3);

        taskInteractor.deleteTask("User 1", task3.id());
        assertThat(taskInteractor.getTasksByFilter(TaskFilter.empty())).isEmpty();
    }

    @Test
    void testShouldNotDeleteTasksAsInvalidUser() {
        Task task = Task.builder().title("Task 1").author("User 1").assignees("User 1", "User 2").fillDefaults();
        taskInteractor.createTask(task);

        assertThatExceptionOfType(InvalidTaskUpdateException.class)
                .isThrownBy(() -> taskInteractor.deleteTask("User 2", task.id()));
        assertThatExceptionOfType(InvalidTaskUpdateException.class)
                .isThrownBy(() -> taskInteractor.deleteTask("User 3", task.id()));
    }
}
