package com.taskify.taskboard.runner;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;
import static com.taskify.taskboard.infra.configurations.Profiles.SECURITY_PROFILE;

@SpringBootTest
@ActiveProfiles({PERSISTENCE_PROFILE, SECURITY_PROFILE})
class TaskBoardApplicationTests {

    @Test
    void contextLoads() {
    }

}
