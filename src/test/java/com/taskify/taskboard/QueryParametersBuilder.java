package com.taskify.taskboard;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public final class QueryParametersBuilder {
    public QueryParametersBuilder() {
        queryParameters = new LinkedMultiValueMap<>();
    }

    public QueryParametersBuilder parameter(String key, String value) {
        queryParameters.add(key, value);
        return this;
    }

    public MultiValueMap<String, String> build() {
        return queryParameters;
    }

    private final MultiValueMap<String, String> queryParameters;
}
