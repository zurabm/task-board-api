package com.taskify.taskboard;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.HttpClientErrorException;

import static org.assertj.core.api.Assertions.assertThat;

public final class TestAssertions {
    /**
     * Validates the HTTP response status is as expected.
     */
    public static void expectStatus(MvcResult result, HttpStatus expectedStatus) {
        assertThat(result.getResponse().getStatus()).isEqualTo(expectedStatus.value());
    }

    /**
     * Validates the HTTP response status is as expected.
     */
    public static void expectStatus(ResponseEntity<?> response, HttpStatus expectedStatus) {
        assertThat(response.getStatusCode()).isEqualTo(expectedStatus);
    }

    /**
     * Validates the HTTP response status is as expected.
     */
    public static void expectStatus(HttpClientErrorException exception, HttpStatus expectedStatus) {
        assertThat(exception.getStatusCode()).isEqualTo(expectedStatus);
    }
}
