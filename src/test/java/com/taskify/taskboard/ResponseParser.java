package com.taskify.taskboard;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;

public final class ResponseParser {
    /**
     * Parses a java object of the specified type from the JSON content of the {@link MvcResult}.
     */
    public static <T> T parseResponse(MvcResult result, Class<T> responseType)
            throws UnsupportedEncodingException, JsonProcessingException {

        return new ObjectMapper().readValue(result.getResponse().getContentAsString(), responseType);
    }
}
