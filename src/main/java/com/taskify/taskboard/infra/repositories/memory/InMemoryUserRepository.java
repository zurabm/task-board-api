package com.taskify.taskboard.infra.repositories.memory;

import com.taskify.taskboard.core.repositories.IUserRepository;
import com.taskify.taskboard.core.users.User;

import java.util.HashMap;
import java.util.List;

public class InMemoryUserRepository implements IUserRepository {
    public InMemoryUserRepository() {
        users = new HashMap<>();
    }

    @Override
    public void addUser(User user) {
        assert !hasUserByEmail(user.email()) : "The email '%s' is already in use.".formatted(user.email());

        users.put(user.email(), user);
    }

    @Override
    public boolean hasUserByEmail(String email) {
        return users.containsKey(email);
    }

    @Override
    public List<User> getUsers() {
        return users.values().stream().toList();
    }

    @Override
    public User getUserByEmail(String email) {
        assert hasUserByEmail(email) : "A user with the email '%s' does not exist.".formatted(email);

        return users.get(email);
    }

    /**
     * Removes all data from this repository.
     */
    public void clear() {
        users.clear();
    }

    private final HashMap<String, User> users;
}
