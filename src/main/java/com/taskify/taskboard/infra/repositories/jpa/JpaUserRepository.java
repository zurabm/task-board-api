package com.taskify.taskboard.infra.repositories.jpa;

import com.taskify.taskboard.core.repositories.IUserRepository;
import com.taskify.taskboard.core.users.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;

@Repository
@Profile(PERSISTENCE_PROFILE)
public class JpaUserRepository implements IUserRepository {
    public JpaUserRepository(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void addUser(User user) {
        userDao.save(UserEntity.from(user));
    }

    @Override
    public boolean hasUserByEmail(String email) {
        return userDao.existsByEmail(email);
    }

    @Override
    public List<User> getUsers() {
        return userDao.findAll().stream().map(UserEntity::toUser).toList();
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.findByEmail(email).toUser();
    }

    private final IUserDao userDao;
}
