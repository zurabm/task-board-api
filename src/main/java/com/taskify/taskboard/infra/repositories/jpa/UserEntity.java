package com.taskify.taskboard.infra.repositories.jpa;

import com.taskify.taskboard.core.users.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = UserEntity.TABLE)
public class UserEntity {
    public static final String TABLE = "users";

    @Id
    private String fullName;
    private String email;
    private String password;

    /**
     * Creates a {@link UserEntity} from the specified {@link User}.
     */
    public static UserEntity from(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.email = user.email();
        userEntity.password = user.password();
        userEntity.fullName = user.fullName();

        return userEntity;
    }

    /**
     * Creates a {@link UserEntity} with only the full name populated.
     */
    public static UserEntity withFullName(String fullName) {
        UserEntity userEntity = new UserEntity();
        userEntity.fullName = fullName;
        return userEntity;
    }

    /**
     * Converts this {@link UserEntity} to a {@link User}.
     */
    public User toUser() {
        return User.builder().email(email).password(password).fullName(fullName).build();
    }

    public String getFullName() {
        return fullName;
    }
}
