package com.taskify.taskboard.infra.repositories.memory;

import com.taskify.taskboard.core.repositories.ITaskRepository;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class InMemoryTaskRepository implements ITaskRepository {
    public InMemoryTaskRepository() {
        tasksById = new HashMap<>();
    }

    @Override
    public void addTask(Task task) {
        assert !hasTaskById(task.id()) : "A task with the ID '%s' already exist.".formatted(task.id());

        tasksById.put(task.id(), task);
    }

    @Override
    public void updateTask(String taskId, Task task) {
        assert hasTaskById(taskId) : "A task with the ID '%s' does not exist.".formatted(taskId);

        tasksById.put(taskId, task);
    }

    @Override
    public Task getTaskById(String taskId) {
        assert hasTaskById(taskId) : "A task with the ID '%s' does not exist.".formatted(taskId);

        return tasksById.get(taskId);
    }

    @Override
    public List<Task> getTasksByFilter(TaskFilter taskFilter, Instant reference) {
        Stream<Task> tasks = tasksById.values().stream();

        if (taskFilter.assignees().isPresent()) {
            Set<String> assignees = taskFilter.assignees().get();
            tasks = tasks.filter(task -> assignees.stream().anyMatch(task.assignees()::contains));
        }

        if (taskFilter.author().isPresent()) {
            String author = taskFilter.author().get();
            tasks = tasks.filter(task -> author.equals(task.author()));
        }

        if (taskFilter.status().isPresent()) {
            tasks = tasks.filter(task -> task.status().equals(taskFilter.status().get()));
        }

        if (taskFilter.overdue().isPresent()) {
            Instant now = reference.plus(taskFilter.timeLeft().orElse(Duration.ZERO));
            tasks = switch (taskFilter.overdue().get()) {
                case NOT_OVERDUE -> tasks.filter(task -> task.due().isAfter(now));
                case OVERDUE -> tasks.filter(task -> task.due().isBefore(now) || task.due().equals(now));
            };
        }

        return tasks.toList();
    }

    @Override
    public void removeTask(String taskId) {
        assert hasTaskById(taskId) : "A task with the ID '%s' does not exist.".formatted(taskId);

        tasksById.remove(taskId);
    }

    /**
     * Returns true if the task with the specified ID exists.
     */
    public boolean hasTask(String taskId) {
        return tasksById.containsKey(taskId);
    }

    /**
     * Removes all data from this repository.
     */
    public void clear() {
        tasksById.clear();
    }

    /**
     * Returns true if the task with the specified ID exists.
     */
    private boolean hasTaskById(String taskId) {
        return tasksById.containsKey(taskId);
    }

    private final Map<String, Task> tasksById;
}
