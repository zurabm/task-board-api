package com.taskify.taskboard.infra.repositories.jpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;

@Repository
@Profile(PERSISTENCE_PROFILE)
public class JpaSettingRepository {
    public JpaSettingRepository(ISettingDao settingDao) {
        this.settingDao = settingDao;
    }

    /**
     * Creates a new setting with the specified name and value.
     */
    public void createSetting(String name, int value) {
        assert !hasSetting(name) : "A setting with the name '%s' does already exists.".formatted(name);

        settingDao.save(SettingEntity.from(name, value));
    }

    /**
     * Returns true if a setting with the specified value exists.
     */
    public boolean hasSetting(String name) {
        return settingDao.existsByName(name);
    }

    /**
     * Returns the value associated with the specified setting.
     */
    public int getSetting(String name) {
        assert hasSetting(name) : "A setting with the name '%s' does not exist.".formatted(name);

        return settingDao.findByName(name).getIntegerValue();
    }

    /**
     * Updates the value for the specified setting.
     */
    public void updateSetting(String name, int value) {
        deleteSetting(name);
        createSetting(name, value);
    }

    /**
     * Deletes the setting with the specified name.
     */
    public void deleteSetting(String name) {
        assert hasSetting(name) : "A setting with the name '%s' does not exist.".formatted(name);

        settingDao.deleteByName(name);
    }

    private final ISettingDao settingDao;
}
