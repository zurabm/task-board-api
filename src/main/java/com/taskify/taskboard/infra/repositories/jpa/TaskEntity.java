package com.taskify.taskboard.infra.repositories.jpa;

import com.taskify.taskboard.core.tasks.Task;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = TaskEntity.TABLE)
public class TaskEntity {
    public static final String TABLE = "tasks";

    @Id
    private String id;
    private String title;
    private String description;
    private Instant due;
    private String status;

    @ManyToOne
    private UserEntity author;

    @ManyToMany
    private Set<UserEntity> assignees;

    /**
     * Creates a {@link TaskEntity} from the specified {@link Task}.
     */
    public static TaskEntity from(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.id = task.id();
        taskEntity.title = task.title();
        taskEntity.description = task.description();
        taskEntity.assignees = task.assignees().stream().map(UserEntity::withFullName).collect(Collectors.toSet());
        taskEntity.author = UserEntity.withFullName(task.author());
        taskEntity.due = task.due();
        taskEntity.status = task.status().name();

        return taskEntity;
    }

    /**
     * Converts this {@link TaskEntity} to a {@link Task}.
     */
    public Task toTask() {
        return Task.builder()
                .id(id)
                .title(title)
                .description(description)
                .due(due)
                .assignees(assignees.stream().map(UserEntity::getFullName).collect(Collectors.toSet()))
                .author(author.getFullName())
                .status(Task.Status.valueOf(status))
                .build();
    }

    public Set<String> getAssignees() {
        return assignees.stream().map(UserEntity::getFullName).collect(Collectors.toSet());
    }
}
