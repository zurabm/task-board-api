package com.taskify.taskboard.infra.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ITaskDao extends JpaRepository<TaskEntity, String>, JpaSpecificationExecutor<TaskEntity> {
}
