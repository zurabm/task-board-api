package com.taskify.taskboard.infra.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserDao extends JpaRepository<UserEntity, String> {
    /**
     * Returns true if a user with the specified email exists.
     */
    boolean existsByEmail(String email);

    /**
     * Retrieves the user with the specified email.
     */
    UserEntity findByEmail(String email);
}
