package com.taskify.taskboard.infra.repositories.jpa;

import javax.persistence.*;

@Entity
@Table(name = SettingEntity.TABLE)
public class SettingEntity {
    public static final String TABLE = "settings";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String name;
    private int integerValue;

    /**
     * Creates a setting entity from the specified key-value pair.
     */
    public static SettingEntity from(String name, int value) {
        SettingEntity entity = new SettingEntity();
        entity.name = name;
        entity.integerValue = value;
        return entity;
    }

    public int getIntegerValue() {
        return integerValue;
    }
}
