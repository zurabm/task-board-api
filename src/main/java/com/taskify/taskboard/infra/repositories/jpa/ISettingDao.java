package com.taskify.taskboard.infra.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ISettingDao extends JpaRepository<SettingEntity, Integer> {
    /**
     * Returns true if a setting with the specified name exists.
     */
    boolean existsByName(String name);

    /**
     * Returns the setting with the specified name.
     */
    SettingEntity findByName(String name);

    /**
     * Deletes the setting with the specified name.
     */
    void deleteByName(String name);
}
