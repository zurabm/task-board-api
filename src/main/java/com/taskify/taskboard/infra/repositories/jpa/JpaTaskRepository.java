package com.taskify.taskboard.infra.repositories.jpa;

import com.taskify.taskboard.core.repositories.ITaskRepository;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;
import static java.util.Collections.disjoint;

@Repository
@Profile(PERSISTENCE_PROFILE)
public class JpaTaskRepository implements ITaskRepository {
    public JpaTaskRepository(ITaskDao taskDao) {
        this.taskDao = taskDao;
    }

    @Override
    public void addTask(Task task) {
        taskDao.save(TaskEntity.from(task));
    }

    @Override
    public void updateTask(String taskId, Task task) {
        assert taskId.equals(task.id()) :
                "The ID '%s' differs from the task's embedded ID '%s'".formatted(taskId, task.id());

        removeTask(taskId);
        addTask(task);
    }

    @Override
    public Task getTaskById(String taskId) {
        assert hasTaskById(taskId) : "A task with the ID '%s' does not exist.".formatted(taskId);

        return taskDao.findById(taskId).orElseThrow().toTask();
    }

    @Override
    public List<Task> getTasksByFilter(TaskFilter taskFilter, Instant reference) {
        Specification<TaskEntity> specification = any();

        if (taskFilter.author().isPresent()) {
            specification = specification.and(withAuthor(taskFilter.author().get()));
        }

        if (taskFilter.status().isPresent()) {
            specification = specification.and(withStatus(taskFilter.status().get()));
        }

        if (taskFilter.overdue().isPresent()) {
            Instant now = reference.plus(taskFilter.timeLeft().orElse(Duration.ZERO));
            specification = switch (taskFilter.overdue().get()) {
                case NOT_OVERDUE -> isNotOverdue(now);
                case OVERDUE -> isOverdue(now);
            };
        }

        // TODO: Rewrite with Criteria API.
        Stream<TaskEntity> tasks = taskDao.findAll(specification).stream();

        if (taskFilter.assignees().isPresent()) {
            var assignees = taskFilter.assignees().get();
            tasks = tasks.filter(task -> !disjoint(task.getAssignees(), assignees));
        }

        return tasks.map(TaskEntity::toTask).toList();
    }

    @Override
    public void removeTask(String taskId) {
        taskDao.deleteById(taskId);
    }

    public boolean hasTaskById(String taskId) {
        return taskDao.existsById(taskId);
    }

    /**
     * Returns a {@link Specification} matching any {@link TaskEntity}.
     */
    private static Specification<TaskEntity> any() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and();
    }

    /**
     * Returns a {@link Specification} matching only {@link TaskEntity}s that have the specified author.
     */
    private static Specification<TaskEntity> withAuthor(String author) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(
                root.get(TaskEntity_.author).get(UserEntity_.fullName), author
        );
    }

    /**
     * Returns a {@link Specification} matching only {@link TaskEntity}s that have the specified status.
     */
    private static Specification<TaskEntity> withStatus(Task.Status status) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(TaskEntity_.status), status.name());
    }

    /**
     * Returns a {@link Specification} matching only {@link TaskEntity}s that are overdue, relative to the specified
     * reference time.
     */
    private static Specification<TaskEntity> isOverdue(Instant reference) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(
                root.get(TaskEntity_.due), reference
        );
    }

    /**
     * Returns a {@link Specification} matching only {@link TaskEntity}s that are not overdue, relative to the specified
     * reference time.
     */
    private static Specification<TaskEntity> isNotOverdue(Instant reference) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(
                root.get(TaskEntity_.due), reference
        );
    }

    private final ITaskDao taskDao;
}
