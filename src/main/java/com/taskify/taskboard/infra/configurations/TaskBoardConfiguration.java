package com.taskify.taskboard.infra.configurations;

import com.taskify.taskboard.core.IdSequence;
import com.taskify.taskboard.core.TaskBoardService;
import com.taskify.taskboard.core.repositories.ITaskRepository;
import com.taskify.taskboard.core.repositories.IUserRepository;
import com.taskify.taskboard.core.tasks.TaskInteractor;
import com.taskify.taskboard.core.users.UserInteractor;
import com.taskify.taskboard.infra.PrefixedIdSequence;
import com.taskify.taskboard.infra.repositories.jpa.JpaSettingRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;

@Configuration
public class TaskBoardConfiguration {
    private static final String TASK_ID_PREFIX = "TB-";

    public TaskBoardConfiguration(
            IUserRepository userRepository, ITaskRepository taskRepository, IdSequence idSequence
    ) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
        this.idSequence = idSequence;
    }

    @Bean
    public TaskBoardService getTaskBoardService() {
        return new TaskBoardService(
                new UserInteractor(userRepository),
                new TaskInteractor(taskRepository, idSequence)
        );
    }

    @Bean
    @Profile(PERSISTENCE_PROFILE)
    public static IdSequence getIdSequence(JpaSettingRepository settingRepository) {
        return PrefixedIdSequence.withPrefix(TASK_ID_PREFIX, settingRepository);
    }

    private final IUserRepository userRepository;
    private final ITaskRepository taskRepository;
    private final IdSequence idSequence;
}
