package com.taskify.taskboard.infra.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import static com.taskify.taskboard.infra.configurations.Profiles.SECURITY_PROFILE;

@EnableWebSecurity
@Profile(SECURITY_PROFILE)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final RequestMatcher PROTECTED_RESOURCES = new OrRequestMatcher(
            new AntPathRequestMatcher("/tasks/**"),
            new AntPathRequestMatcher("/users/**", HttpMethod.GET.name()),
            new AntPathRequestMatcher("/sessions/**", HttpMethod.GET.name()),
            new AntPathRequestMatcher("/sessions/**", HttpMethod.DELETE.name())
    );

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().requestMatchers(PROTECTED_RESOURCES).authenticated();

        http.cors().and().csrf().disable();
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public static PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
