package com.taskify.taskboard.infra.configurations;

public final class Profiles {
    public static final String IN_MEMORY_PROFILE = "in-memory";
    public static final String PERSISTENCE_PROFILE = "persistence";

    public static final String NO_SECURITY_PROFILE = "no-security";
    public static final String SECURITY_PROFILE = "security";
}
