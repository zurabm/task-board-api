package com.taskify.taskboard.infra;

import com.taskify.taskboard.core.IdSequence;
import com.taskify.taskboard.infra.repositories.jpa.JpaSettingRepository;
import org.springframework.context.annotation.Profile;

import static com.taskify.taskboard.infra.configurations.Profiles.PERSISTENCE_PROFILE;

@Profile(PERSISTENCE_PROFILE)
public class PrefixedIdSequence implements IdSequence {
    private static final String SETTING_LAST_ID = "LAST_ID";

    @Override
    public String next() {
        int lastId = settingRepository.getSetting(SETTING_LAST_ID);
        settingRepository.updateSetting(SETTING_LAST_ID, lastId + 1);

        return prefix + lastId;
    }

    /**
     * Creates a new {@link IdSequence} that will have the specified prefix for all generated IDs.
     */
    public static PrefixedIdSequence withPrefix(String prefix, JpaSettingRepository settingRepository) {
        return new PrefixedIdSequence(prefix, settingRepository);
    }

    private PrefixedIdSequence(String prefix, JpaSettingRepository settingRepository) {
        this.prefix = prefix;
        this.settingRepository = settingRepository;

        initializeId();
    }

    /**
     * Creates the initial ID if one does not exist.
     */
    private void initializeId() {
        if (!settingRepository.hasSetting(SETTING_LAST_ID)) {
            settingRepository.createSetting(SETTING_LAST_ID, 1);
        }
    }

    private final String prefix;
    private final JpaSettingRepository settingRepository;
}
