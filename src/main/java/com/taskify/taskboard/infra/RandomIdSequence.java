package com.taskify.taskboard.infra;

import com.taskify.taskboard.core.IdSequence;

import java.util.UUID;

public class RandomIdSequence implements IdSequence {
    @Override
    public String next() {
        return UUID.randomUUID().toString();
    }
}
