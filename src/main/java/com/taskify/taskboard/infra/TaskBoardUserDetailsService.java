package com.taskify.taskboard.infra;

import com.taskify.taskboard.core.TaskBoardService;
import com.taskify.taskboard.core.users.InvalidEmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class TaskBoardUserDetailsService implements UserDetailsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(TaskBoardUserDetailsService.class);

    public TaskBoardUserDetailsService(TaskBoardService taskBoardService) {
        this.taskBoardService = taskBoardService;
    }

    /**
     * Task Board users are authenticated via email.
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            return new TaskBoardUserDetails(email, taskBoardService.getUserByEmail(email).password());
        } catch (InvalidEmailException e) {
            LOGGER.info("Could not find a user: {}", e.getMessage());
            throw new UsernameNotFoundException(e.getMessage());
        }
    }

    private final TaskBoardService taskBoardService;
}
