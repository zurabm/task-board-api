package com.taskify.taskboard.infra.controllers.requests;

import com.taskify.taskboard.infra.controllers.models.TaskModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateTaskRequest {
    private TaskModel task;
}
