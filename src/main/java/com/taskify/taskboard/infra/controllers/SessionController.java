package com.taskify.taskboard.infra.controllers;

import com.taskify.taskboard.core.TaskBoardService;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.controllers.requests.CreateSessionRequest;
import com.taskify.taskboard.infra.controllers.responses.GetSessionResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/sessions")
public class SessionController {
    public static final Logger LOGGER = LoggerFactory.getLogger(SessionController.class);

    public SessionController(TaskBoardService taskBoardService, AuthenticationManager authenticationManager) {
        this.taskBoardService = taskBoardService;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createSession(@RequestBody CreateSessionRequest request) throws BadCredentialsException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())
        );

        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GetSessionResponse getSession(Principal principal) {
        User user = taskBoardService.getUserByEmail(principal.getName());

        return new GetSessionResponse(user.fullName(), user.email());
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteSession() {
        SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void handleBadCredentialsException(BadCredentialsException e) {
        LOGGER.info("Received invalid credentials. Exception: {}", e.getMessage());
    }

    private final TaskBoardService taskBoardService;
    private final AuthenticationManager authenticationManager;
}
