package com.taskify.taskboard.infra.controllers.models;

import com.taskify.taskboard.core.tasks.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskModel {
    private String id;
    private String title;
    private String description;
    private String due;
    private List<String> assignees;
    private String author;
    private Task.Status status;

    /**
     * Creates a {@link TaskModel} from the specified {@link Task}.
     */
    public static TaskModel from(Task task) {
        TaskModel model = new TaskModel();
        model.id = task.id();
        model.title = task.title();
        model.description = task.description();
        model.due = task.due().toString();
        model.assignees = task.assignees().stream().toList();
        model.author = task.author();
        model.status = task.status();

        return model;
    }

    /**
     * Converts this {@link TaskModel} to a {@link Task}.
     */
    public Task toTask() {
        return Task.builder()
                .id(id)
                .title(title)
                .description(description)
                .due(Instant.parse(due))
                .assignees(assignees)
                .author(author)
                .status(status)
                .build();
    }
}
