package com.taskify.taskboard.infra.controllers.requests;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegisterUserRequest {
    private String email;
    private String password;
    private String fullName;
}
