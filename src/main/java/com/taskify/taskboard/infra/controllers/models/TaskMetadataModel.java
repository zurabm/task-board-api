package com.taskify.taskboard.infra.controllers.models;

import com.taskify.taskboard.core.tasks.Task;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskMetadataModel {
    private String id;
    private String title;
    private String due;
    private Task.Status status;

    /**
     * Creates a {@link TaskMetadataModel} from the specified {@link Task}.
     */
    public static TaskMetadataModel from(Task task) {
        TaskMetadataModel model = new TaskMetadataModel();
        model.id = task.id();
        model.title = task.title();
        model.due = task.due().toString();
        model.status = task.status();

        return model;
    }
}
