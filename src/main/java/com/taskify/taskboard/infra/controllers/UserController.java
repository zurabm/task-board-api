package com.taskify.taskboard.infra.controllers;

import com.taskify.taskboard.core.TaskBoardService;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.infra.controllers.models.UserMetadataModel;
import com.taskify.taskboard.infra.controllers.requests.RegisterUserRequest;
import com.taskify.taskboard.infra.controllers.responses.GetUsersResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    public UserController(TaskBoardService taskBoardService) {
        this.taskBoardService = taskBoardService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void registerUser(@RequestBody RegisterUserRequest request) {
        taskBoardService.registerUser(User.builder()
                .email(request.getEmail())
                .password(request.getPassword())
                .fullName(request.getFullName())
                .build());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GetUsersResponse getUsers() {
        return new GetUsersResponse(taskBoardService.getUsers().stream().map(UserMetadataModel::from).toList());
    }

    private final TaskBoardService taskBoardService;
}
