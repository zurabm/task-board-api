package com.taskify.taskboard.infra.controllers.responses;

import com.taskify.taskboard.infra.controllers.models.TaskMetadataModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetTasksResponse {
    private List<TaskMetadataModel> tasks;
}
