package com.taskify.taskboard.infra.controllers.requests;

import com.taskify.taskboard.core.tasks.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTaskRequest {
    private List<String> assignees;
    private Task.Status status;
}
