package com.taskify.taskboard.infra.controllers;

import com.taskify.taskboard.core.TaskBoardService;
import com.taskify.taskboard.core.tasks.InvalidTaskUpdateException;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;
import com.taskify.taskboard.infra.controllers.models.TaskMetadataModel;
import com.taskify.taskboard.infra.controllers.models.TaskModel;
import com.taskify.taskboard.infra.controllers.requests.CreateTaskRequest;
import com.taskify.taskboard.infra.controllers.requests.UpdateTaskRequest;
import com.taskify.taskboard.infra.controllers.responses.CreateTaskResponse;
import com.taskify.taskboard.infra.controllers.responses.GetTaskResponse;
import com.taskify.taskboard.infra.controllers.responses.GetTasksResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    public static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

    public TaskController(TaskBoardService taskBoardService) {
        this.taskBoardService = taskBoardService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public CreateTaskResponse createTask(@RequestBody CreateTaskRequest request, Principal principal) {
        TaskModel taskModel = request.getTask();

        String author = taskBoardService.getUserByEmail(principal.getName()).fullName();
        String taskId = taskBoardService.createTaskId();

        var taskBuilder = Task.builder()
                .id(taskId)
                .title(taskModel.getTitle())
                .description(taskModel.getDescription())
                .author(author);

        Optional.ofNullable(taskModel.getDue()).ifPresent(due -> taskBuilder.due(Instant.parse(due)));
        Optional.ofNullable(taskModel.getAssignees()).ifPresent(taskBuilder::assignees);
        Optional.ofNullable(taskModel.getStatus()).ifPresent(taskBuilder::status);

        taskBoardService.createTask(taskBuilder.build());

        return new CreateTaskResponse(taskId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GetTasksResponse getTasks(
            @RequestParam Optional<String[]> assignees,
            @RequestParam Optional<String> author,
            @RequestParam Optional<String> status,
            @RequestParam Optional<String> overdue,
            @RequestParam("time-left") Optional<String> timeLeft
    ) {
        var filterBuilder = TaskFilter.builder();

        assignees.ifPresent(filterBuilder::assignees);
        author.ifPresent(filterBuilder::author);
        status.ifPresent(presentStatus -> filterBuilder.status(Task.Status.valueOf(presentStatus)));
        overdue.ifPresent(presentOverdue -> filterBuilder.overdue(TaskFilter.Overdue.valueOf(presentOverdue)));
        timeLeft.ifPresent(presentTimeLeft -> filterBuilder.timeLeft(Duration.parse(presentTimeLeft)));

        return new GetTasksResponse(
                taskBoardService.getTasksByFilter(filterBuilder.build()).stream().map(TaskMetadataModel::from).toList()
        );
    }

    @GetMapping("/{taskId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GetTaskResponse getTask(@PathVariable String taskId) {
        return new GetTaskResponse(TaskModel.from(taskBoardService.getTaskById(taskId)));
    }

    @PutMapping("/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTask(@PathVariable String taskId, @RequestBody UpdateTaskRequest request, Principal principal) {
        String author = taskBoardService.getUserByEmail(principal.getName()).fullName();

        Optional.ofNullable(request.getAssignees()).ifPresent(
                assignees -> taskBoardService.updateTaskAssignees(author, taskId, assignees)
        );

        Optional.ofNullable(request.getStatus()).ifPresent(
                status -> taskBoardService.updateTaskStatus(author, taskId, status)
        );
    }

    @DeleteMapping("/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTask(@PathVariable String taskId, Principal principal) {
        taskBoardService.deleteTask(taskBoardService.getUserByEmail(principal.getName()).fullName(), taskId);
    }

    @ExceptionHandler(InvalidTaskUpdateException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handleInvalidTaskUpdateException(InvalidTaskUpdateException e) {
        LOGGER.info("Illegal update requested. Exception: {}", e.getMessage());
    }

    private final TaskBoardService taskBoardService;
}
