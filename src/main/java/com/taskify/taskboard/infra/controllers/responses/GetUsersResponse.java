package com.taskify.taskboard.infra.controllers.responses;

import com.taskify.taskboard.infra.controllers.models.UserMetadataModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUsersResponse {
    private List<UserMetadataModel> users;
}
