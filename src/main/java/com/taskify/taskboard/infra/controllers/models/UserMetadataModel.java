package com.taskify.taskboard.infra.controllers.models;

import com.taskify.taskboard.core.users.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserMetadataModel {
    private String fullName;
    private String email;

    /**
     * Creates a {@link UserMetadataModel} from the specified {@link User}.
     */
    public static UserMetadataModel from(User user) {
        UserMetadataModel model = new UserMetadataModel();
        model.fullName = user.fullName();
        model.email = user.email();

        return model;
    }
}
