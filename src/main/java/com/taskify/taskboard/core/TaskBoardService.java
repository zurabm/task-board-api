package com.taskify.taskboard.core;

import com.taskify.taskboard.core.tasks.InvalidTaskUpdateException;
import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;
import com.taskify.taskboard.core.tasks.TaskInteractor;
import com.taskify.taskboard.core.users.InvalidEmailException;
import com.taskify.taskboard.core.users.User;
import com.taskify.taskboard.core.users.UserInteractor;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class TaskBoardService {
    public TaskBoardService(UserInteractor userInteractor, TaskInteractor taskInteractor) {
        this.userInteractor = userInteractor;
        this.taskInteractor = taskInteractor;
    }

    /**
     * Registers a new user.
     */
    public void registerUser(User user) {
        userInteractor.registerUser(user);
    }

    /**
     * Returns all registered users.
     */
    public List<User> getUsers() {
        return userInteractor.getUsers();
    }

    /**
     * Retrieves the user with the specified email.
     *
     * @throws InvalidEmailException if a user with the specified email does not exist.
     */
    public User getUserByEmail(String email) {
        return userInteractor.getUserByEmail(email);
    }

    /**
     * Creates a unique task ID.
     */
    public String createTaskId() {
        return UUID.randomUUID().toString();
    }

    /**
     * Creates a new task according to the specified task details.
     */
    public void createTask(Task task) {
        taskInteractor.createTask(task);
    }

    /**
     * Returns the task with the specified ID.
     */
    public Task getTaskById(String taskId) {
        return taskInteractor.getTaskById(taskId);
    }

    /**
     * Returns a {@link List} of all tasks that satisfy the specified filter.
     */
    public List<Task> getTasksByFilter(TaskFilter filter) {
        return taskInteractor.getTasksByFilter(filter);
    }

    /**
     * Updates the assignee list of the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the update author is not the author of the task, or a current assignee.
     */
    public void updateTaskAssignees(String updateAuthor, String taskId, Collection<String> assignees) {
        taskInteractor.updateTaskAssignees(updateAuthor, taskId, assignees);
    }

    /**
     * Updates the status of the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the update author is not the author of the task, or a current assignee.
     */
    public void updateTaskStatus(String updateAuthor, String taskId, Task.Status status) {
        taskInteractor.updateTaskStatus(updateAuthor, taskId, status);
    }

    /**
     * Deletes the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the deletion author is not the author of the task.
     */
    public void deleteTask(String deleteAuthor, String taskId) {
        taskInteractor.deleteTask(deleteAuthor, taskId);
    }

    private final UserInteractor userInteractor;
    private final TaskInteractor taskInteractor;
}
