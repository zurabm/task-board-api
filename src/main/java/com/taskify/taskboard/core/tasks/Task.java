package com.taskify.taskboard.core.tasks;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static java.util.Objects.requireNonNull;

public record Task(
        String id,
        String title,
        String description,
        Instant due,
        Set<String> assignees,
        String author,
        Status status
) {
    public static final String TITLE_NONE = "";
    public static final String DESCRIPTION_NONE = "";
    public static final Instant DUE_DATE_NEVER = LocalDateTime.MAX.minusYears(1).toInstant(ZoneOffset.UTC);
    public static final String AUTHOR_NONE = "";
    public static final Set<String> ASSIGNEES_NONE = Collections.emptySet();

    public enum Status {
        NOT_DONE, IN_PROGRESS, DONE
    }

    public static class TaskBuilder {
        private String id;
        private String title;
        private String description;
        private Instant due;
        private Set<String> assignees;
        private String author;
        private Status status;

        /**
         * Sets the unique ID for this {@link TaskBuilder}.
         */
        public TaskBuilder id(String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the title for this {@link TaskBuilder}.
         */
        public TaskBuilder title(String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the description for this {@link TaskBuilder}.
         */
        public TaskBuilder description(String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the due date for this {@link TaskBuilder}.
         */
        public TaskBuilder due(Instant due) {
            this.due = due;
            return this;
        }

        /**
         * Sets the assignees for this {@link TaskBuilder}.
         */
        public TaskBuilder assignees(Collection<String> assignees) {
            this.assignees = Set.copyOf(assignees);
            return this;
        }

        /**
         * Sets the assignees for this {@link TaskBuilder}. Convenience overload with variable arguments.
         */
        public TaskBuilder assignees(String... assignees) {
            return assignees(Arrays.stream(assignees).toList());
        }

        /**
         * Sets the author for this {@link TaskBuilder}.
         */
        public TaskBuilder author(String author) {
            this.author = author;
            return this;
        }

        /**
         * Sets the status for this {@link TaskBuilder}.
         */
        public TaskBuilder status(Status status) {
            this.status = status;
            return this;
        }

        /**
         * Creates a {@link Task} with the current state of this {@link TaskBuilder}.
         */
        public Task build() {
            requireNonNull(id);
            requireNonNull(title);
            requireNonNull(description);
            requireNonNull(due);
            requireNonNull(assignees);
            requireNonNull(author);
            requireNonNull(status);

            return new Task(id, title, description, due, assignees, author, status);
        }

        /**
         * Creates a {@link Task} with default values from the this {@link Task.TaskBuilder}.
         */
        public Task fillDefaults() {
            return this
                    .id(id == null ? UUID.randomUUID().toString() : id)
                    .title(title == null ? TITLE_NONE : title)
                    .description(description == null ? DESCRIPTION_NONE : description)
                    .due(due == null ? Task.DUE_DATE_NEVER : due)
                    .assignees(assignees == null ? ASSIGNEES_NONE : assignees)
                    .author(author == null ? AUTHOR_NONE : author)
                    .status(status == null ? Status.NOT_DONE : status)
                    .build();
        }
    }

    /**
     * Creates a new {@link TaskBuilder} from this {@link Task}.
     */
    public TaskBuilder toBuilder() {
        return new TaskBuilder()
                .id(id)
                .title(title)
                .description(description)
                .due(due)
                .assignees(assignees)
                .author(author)
                .status(status);
    }

    /**
     * Creates a new {@link TaskBuilder}.
     */
    public static TaskBuilder builder() {
        return new TaskBuilder();
    }
}
