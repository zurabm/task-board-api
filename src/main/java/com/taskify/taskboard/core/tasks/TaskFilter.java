package com.taskify.taskboard.core.tasks;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public record TaskFilter(
        Optional<Set<String>> assignees,
        Optional<String> author,
        Optional<Task.Status> status,
        Optional<Overdue> overdue,
        Optional<Duration> timeLeft
) {
    private static final TaskFilter EMPTY = TaskFilter.builder().build();

    public enum Overdue {
        NOT_OVERDUE, OVERDUE
    }

    public static class TaskFilterBuilder {
        private Set<String> assignees;
        private String author;
        private Task.Status status;
        private Overdue overdue;
        private Duration timeLeft;

        /**
         * Sets the assignees filter for this {@link TaskFilterBuilder}.
         */
        public TaskFilterBuilder assignees(Collection<String> assignees) {
            this.assignees = Set.copyOf(assignees);
            return this;
        }

        /**
         * Sets the author filter for this {@link TaskFilterBuilder}.
         */
        public TaskFilterBuilder author(String author) {
            this.author = author;
            return this;
        }

        /**
         * Sets the assignees filter for this {@link TaskFilterBuilder}. Convenience overload with variable arguments.
         */
        public TaskFilterBuilder assignees(String... assignees) {
            return assignees(Arrays.stream(assignees).toList());
        }

        /**
         * Sets the status filter for this {@link TaskFilterBuilder}.
         */
        public TaskFilterBuilder status(Task.Status status) {
            this.status = status;
            return this;
        }

        /**
         * Sets the overdue filter for this {@link TaskFilterBuilder}.
         */
        public TaskFilterBuilder overdue(Overdue overdue) {
            this.overdue = overdue;
            return this;
        }

        /**
         * Sets the maximum time left until a task is overdue for this {@link TaskFilterBuilder}.
         */
        public TaskFilterBuilder timeLeft(Duration timeLeft) {
            this.timeLeft = timeLeft;
            return this;
        }

        /**
         * Creates a {@link TaskFilter} with the current state of this {@link TaskFilterBuilder}.
         */
        public TaskFilter build() {
            return new TaskFilter(
                    Optional.ofNullable(assignees),
                    Optional.ofNullable(author),
                    Optional.ofNullable(status),
                    Optional.ofNullable(overdue),
                    Optional.ofNullable(timeLeft)
            );
        }
    }

    /**
     * Creates a new {@link TaskFilterBuilder}.
     */
    public static TaskFilterBuilder builder() {
        return new TaskFilterBuilder();
    }

    /**
     * Creates an empty filter that matches any task.
     */
    public static TaskFilter empty() {
        return EMPTY;
    }
}
