package com.taskify.taskboard.core.tasks;

public class InvalidTaskUpdateException extends RuntimeException {
    public InvalidTaskUpdateException(String message) {
        super(message);
    }
}
