package com.taskify.taskboard.core.tasks;

import com.taskify.taskboard.core.IdSequence;
import com.taskify.taskboard.core.repositories.ITaskRepository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

public record TaskInteractor(ITaskRepository taskRepository, IdSequence idSequence) {
    /**
     * Creates a unique task ID.
     */
    public String createTaskId() {
        return idSequence.next();
    }

    /**
     * Creates a new task according to the specified task details.
     */
    public void createTask(Task task) {
        taskRepository.addTask(task);
    }

    /**
     * Returns the task with the specified ID.
     */
    public Task getTaskById(String taskId) {
        return taskRepository.getTaskById(taskId);
    }

    /**
     * Returns a {@link List} of all tasks that satisfy the specified filter.
     */
    public List<Task> getTasksByFilter(TaskFilter filter) {
        return taskRepository.getTasksByFilter(filter, Instant.now());
    }

    /**
     * Updates the assignee list of the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the update author is not the author of the task, or a current assignee.
     */
    public void updateTaskAssignees(String updateAuthor, String taskId, Collection<String> assignees) {
        Task task = taskRepository.getTaskById(taskId);

        if (!task.author().equals(updateAuthor) && !task.assignees().contains(updateAuthor)) {
            throw new InvalidTaskUpdateException(
                    "The user '%s' is not the author, nor an assignee for task '%s'.".formatted(updateAuthor, taskId)
            );
        }

        taskRepository.updateTask(taskId, task.toBuilder().assignees(assignees).build());
    }

    /**
     * Updates the status of the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the update author is not the author of the task, or a current assignee.
     */
    public void updateTaskStatus(String updateAuthor, String taskId, Task.Status status) {
        Task task = taskRepository.getTaskById(taskId);

        if (!task.author().equals(updateAuthor) && !task.assignees().contains(updateAuthor)) {
            throw new InvalidTaskUpdateException(
                    "The user '%s' is not the author, nor an assignee for task '%s'.".formatted(updateAuthor, taskId)
            );
        }

        taskRepository.updateTask(taskId, task.toBuilder().status(status).build());
    }

    /**
     * Deletes the task with the specified ID.
     *
     * @throws InvalidTaskUpdateException if the deletion author is not the author of the task.
     */
    public void deleteTask(String deleteAuthor, String taskId) {
        Task task = taskRepository.getTaskById(taskId);

        if (!task.author().equals(deleteAuthor)) {
            throw new InvalidTaskUpdateException(
                    "The user '%s' is not the author for the task '%s'.".formatted(deleteAuthor, taskId)
            );
        }

        taskRepository.removeTask(taskId);
    }
}
