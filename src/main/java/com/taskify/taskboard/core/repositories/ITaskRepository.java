package com.taskify.taskboard.core.repositories;

import com.taskify.taskboard.core.tasks.Task;
import com.taskify.taskboard.core.tasks.TaskFilter;

import java.time.Instant;
import java.util.List;

public interface ITaskRepository {
    /**
     * Adds the specified task to this repository.
     */
    void addTask(Task task);

    /**
     * Updates the task with the specified ID.
     */
    void updateTask(String taskId, Task task);

    /**
     * Returns the task with the specified ID.
     */
    Task getTaskById(String taskId);

    /**
     * Returns a {@link List} of all tasks that satisfy the specified {@link TaskFilter}. The reference time
     * to be used while filtering should also be specified.
     */
    List<Task> getTasksByFilter(TaskFilter taskFilter, Instant reference);

    /**
     * Removes the task with the specified ID.
     */
    void removeTask(String taskId);
}
