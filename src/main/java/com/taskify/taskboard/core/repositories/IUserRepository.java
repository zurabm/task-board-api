package com.taskify.taskboard.core.repositories;

import com.taskify.taskboard.core.users.User;

import java.util.List;

public interface IUserRepository {
    /**
     * Adds the specified user to the repository.
     */
    void addUser(User user);

    /**
     * Returns true if a user with the specified email exists.
     */
    boolean hasUserByEmail(String email);

    /**
     * Returns all users in this repository.
     */
    List<User> getUsers();

    /**
     * Retrieves the user with the specified email.
     */
    User getUserByEmail(String email);
}
