package com.taskify.taskboard.core.users;

import com.taskify.taskboard.core.repositories.IUserRepository;

import java.util.List;

public record UserInteractor(IUserRepository userRepository) {
    /**
     * Registers a new user.
     */
    public void registerUser(User user) {
        userRepository.addUser(user);
    }

    /**
     * Returns all registered users.
     */
    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    /**
     * Retrieves the user with the specified email.
     *
     * @throws InvalidEmailException if a user with the specified email does not exist.
     */
    public User getUserByEmail(String email) {
        if (!userRepository.hasUserByEmail(email)) {
            throw new InvalidEmailException("A user with the email '%s' does not exist.".formatted(email));
        }

        return userRepository.getUserByEmail(email);
    }
}
