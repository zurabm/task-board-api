package com.taskify.taskboard.core.users;

import static java.util.Objects.requireNonNull;

public record User(String email, String password, String fullName) {
    public static class UserBuilder {
        private String email;
        private String password;
        private String fullName;

        /**
         * Sets the email for this {@link UserBuilder}.
         */
        public UserBuilder email(String email) {
            this.email = email;
            return this;
        }

        /**
         * Sets the password for this {@link UserBuilder}.
         */
        public UserBuilder password(String password) {
            this.password = password;
            return this;
        }

        /**
         * Sets the full name for this {@link UserBuilder}.
         */
        public UserBuilder fullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        /**
         * Creates a {@link User} with the current state of this {@link UserBuilder}.
         */
        public User build() {
            requireNonNull(email);
            requireNonNull(password);
            requireNonNull(fullName);

            return new User(email, password, fullName);
        }
    }

    /**
     * Creates a new {@link UserBuilder}.
     */
    public static UserBuilder builder() {
        return new UserBuilder();
    }
}
