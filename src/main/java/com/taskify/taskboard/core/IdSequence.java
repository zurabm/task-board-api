package com.taskify.taskboard.core;

@FunctionalInterface
public interface IdSequence {
    /**
     * Returns the next ID in the sequence, guaranteed to be different from all previous IDs.
     */
    String next();
}
